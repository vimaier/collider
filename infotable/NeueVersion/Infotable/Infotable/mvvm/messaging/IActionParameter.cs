﻿
namespace Infotable.mvvm.messaging
{
    interface IActionParameter
    {
        void ExecuteWithParameter(object parameter);
    }
}
