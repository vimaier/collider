﻿using System;

namespace Infotable_Admin.viewmodel
{
    public static class ViewModelNames
    {
        public const String MAIN_VIEW_NAME = "MainView";
        public const String LOGIN_VIEW_NAME = "LoginView";
    }
}
