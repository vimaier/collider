﻿using Infotable_Admin.model.enums;
using Infotable_Admin.mvvm.model;
using Infotable_Admin.service.log;
using System;
using System.IO;
using System.Windows.Media.Imaging;

namespace Infotable_Admin.model
{
    public class Datei : ModelBase
    {
        #region Members

        private string pfad;
        private string verzeichnis;
        private string name;

        private Data data;
        private BitmapImage image;
        
        #endregion

        #region Constructors
        public Datei()
        {
            this.Clear();
        }

        public Datei(string pfad, string verzeichnis, string name, Data data)
        {
            this.pfad = pfad;
            this.verzeichnis = verzeichnis;
            this.name = name;

            this.data = data;

            if (data == enums.Data.Bild)
            {
                BitmapImage img = new BitmapImage();
                try
                {
                    img.BeginInit();
                    img.CacheOption = BitmapCacheOption.OnLoad;
                    img.UriSource = new Uri(pfad);
                    img.EndInit();
                }
                catch { }

                this.image = img;
            }
        }

        public Datei(Datei d)
        {
            this.pfad = d.pfad;
            this.verzeichnis = d.verzeichnis;
            this.name = d.name;

            this.data = d.data;
            this.image = d.image;
        }

        #endregion

        #region Properties

        public string Pfad
        {
            get { return pfad; }
            set
            {
                if (pfad == value)
                    return;
                pfad = value;
                RaisePropertyChanged("Pfad");
            }
        }

        public string Verzeichnis
        {
            get { return verzeichnis; }
            set
            {
                if (verzeichnis == value)
                    return;
                verzeichnis = value;
                RaisePropertyChanged("verzeichnis");
            }
        }

        public string Name
        {
            get { return name; }
            set
            {
                if (name == value)
                    return;
                name = value;
                RaisePropertyChanged("Name");
            }
        }

        public Data Data
        {
            get { return data; }
            set
            {
                if (data == value)
                    return;
                data = value;
                RaisePropertyChanged("Data");
            }
        }

        public BitmapImage Image
        {
            get { return image; }
            set
            {
                if (image == value)
                    return;
                image = value;
                RaisePropertyChanged("Image");
            }
        }

        #endregion

        #region Methods

        public void Clear()
        {
            this.pfad = string.Empty;
            this.verzeichnis = string.Empty;
            this.name = string.Empty;

            this.data = enums.Data.Empty;
            this.image = null;
        }

        #endregion
    }
}
