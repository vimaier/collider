﻿using Infotable.mvvm.model;
using System;
using System.Windows;

namespace Infotable.model
{
    public class NewsTicker : ModelBase
    {
        #region Members

        private string content;
        private int width;
        private int from;
        private int to;
        private Duration duration;
        
        #endregion

        #region Constructors

        public NewsTicker()
        {
            this.Clear();
        }

        #endregion

        #region Properties

        public string Content
        {
            get { return content; }
            set
            {
                if (content == value)
                    return;
                content = value;
                RaisePropertyChanged("Content");
            }
        }

        public int Width
        {
            get { return width; }
            set
            {
                if (width == value)
                    return;
                width = value;
                RaisePropertyChanged("Width");
            }
        }

        public int From
        {
            get { return from; }
            set
            {
                if (from == value)
                    return;
                from = value;
                RaisePropertyChanged("From");
            }
        }

        public int To
        {
            get { return to; }
            set
            {
                if (to == value)
                    return;
                to = value;
                RaisePropertyChanged("To");
            }
        }

        public Duration Duration
        {
            get { return duration; }
            set
            {
                if (duration == value)
                    return;
                duration = value;
                RaisePropertyChanged("Duration");
            }
        }

        #endregion

        #region Methods

        public void Clear()
        {
            this.content = string.Empty;
            this.width = 0;
            this.from = 0;
            this.to = 0;
            this.duration = new Duration(new TimeSpan(0));
        }

        #endregion
    }
}
