﻿using Infotable.mvvm.notification;

namespace Infotable.mvvm.viewmodel
{
    /// <summary>
    /// the base class for the viewmodels
    /// </summary>
    public class ViewModelBase : NotificationBase
    {
    }
}
