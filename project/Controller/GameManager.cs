﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Threading;
using ProtonCollider.View;
using ProtonCollider.Model;
using System.Windows.Media.Animation;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Microsoft.Surface.Presentation.Controls;
using System.Windows.Media;

namespace ProtonCollider.Controller
{

    /// <summary>
    /// Class that controls the game setting. 
    /// Used to load a level and manage it's environment while playing (Timer, Pause, etc.)
    /// </summary>
    class GameManager
    {
        private const int _TIME_RUNNING_OUT_AT = 10; //changing timer to red at X seconds left

        /// <summary>
        /// Singleton Instance
        /// </summary>
        private static GameManager instance;
        public static GameManager Instance
        {
            get
            {
                if (null == instance)
                    instance = new GameManager();
                return instance;
            }

            private set { }
        }

        private PauseMenu pauseMenu;
        private DispatcherTimer levelTimer;
        private TimeSpan levelTimerVal;
        private Level currentLevel;
        private bool timeIsRunningOut;

        private GameManager()
        {
            InitTimer();
            InitPauseMenu();
            InitProtons();
        }

        public PauseMenu PauseMenu
        {
            get { return this.pauseMenu; }
            private set { }
        }

        public bool TimeIsRunningOut
        {
            get { return timeIsRunningOut; }
            private set { }
        }

        /// <summary>
        /// Brings the protons to the view and start collision detection
        /// </summary>
        private void InitProtons()
        {
            foreach (Proton proton in ProtonManager.Instance.PlayerOneProtons())
            {
                MainWindow.Instance.GameAreaScatterView.Items.Add(proton);
            }
            foreach (Proton proton in ProtonManager.Instance.PlayerTwoProtons())
            {
                MainWindow.Instance.GameAreaScatterView.Items.Add(proton);
            }

            ProtonManager.Instance.StartDetection();
            ProtonManager.Instance.ShowOneProtonPerPlayer();
        }

        /// <summary>
        /// Bereitet den Countdown-Timer vor
        /// </summary>
        private void InitTimer()
        {
            levelTimer = new DispatcherTimer();
            levelTimer.Tick += new EventHandler(LevelTimer_Tick);
            levelTimer.Interval = TimeSpan.FromMilliseconds(47);
        }

        /// <summary>
        /// Timer Tick Eventhandler. Wird ein mal pro Intervall aufgerufen.
        /// Dekrementiert den Timerwert, aktualisiert dessen Anzeige und achtet auf Erreichen des Zeitlimits
        /// </summary>
        private void LevelTimer_Tick(object sender, EventArgs e)
        {
            // Timer Ende Bedingung
            if (levelTimerVal <= TimeSpan.Zero)
            {
                EndLevel();
            }

            // Timer dekrementieren
            levelTimerVal = levelTimerVal.Add(TimeSpan.FromMilliseconds(-47));

            // Zeitalarm setzen?
            this.timeIsRunningOut = levelTimerVal.TotalSeconds < _TIME_RUNNING_OUT_AT && levelTimerVal.TotalSeconds > 0;

            // Timer ausgeben
            ViewManager.Instance.GameView.UpdateTimerValue(levelTimerVal <= TimeSpan.Zero ? TimeSpan.Zero : levelTimerVal, this.timeIsRunningOut);
        }

        /// <summary>
        /// Bereitet das Pausemenue vor und weist dessen Buttons die Resume, Restart und BackToMain Methoden zu
        /// </summary>
        private void InitPauseMenu()
        {
            this.pauseMenu = new PauseMenu(Resume, Restart, BackToMain);
        }

        /// <summary>
        /// Beendet das Level und kehrt zum Hauptmenue zurueck
        /// </summary>
        public void BackToMain()
        {
            ViewManager.Instance.GameView.ContentHolder.Content = null;
            this.pauseMenu.IsActive = false;
            ProtonManager.Instance.DestroyProtons();
            ProtonManager.Instance.StopDetection();
            ProtonManager.Instance.HideProtons();
            ViewManager.Instance.ChangeView(ViewManager.Views.MainMenu);
            MainWindow.Instance.PauseBGVideo();
        }

        /// <summary>
        /// Pausiert das Level und zeigt das Pausemenue an
        /// </summary>
        public void Pause()
        {
            ViewManager.Instance.GameView.ContentHolder.Content = pauseMenu;
            this.pauseMenu.IsActive = true;
            this.levelTimer.Stop();
            ProtonManager.Instance.StopDetection();
            ProtonManager.Instance.HideProtons();
            MainWindow.Instance.PauseBGVideo();
            SoundManager.Instance.RoundMusic.Pause();

            //pause running out sound
            if (this.timeIsRunningOut)
                SoundManager.Instance.RunningOutSound.Pause();
        }

        /// <summary>
        /// Nimmt das Spiel wieder auf und schliesst das Pausemenue
        /// </summary>
        public void Resume()
        {
            ViewManager.Instance.GameView.ContentHolder.Content = null;
            this.pauseMenu.IsActive = false;
            this.levelTimer.Start();
            ProtonManager.Instance.StartDetection();
            ProtonManager.Instance.ShowOneProtonPerPlayer();

            MainWindow.Instance.ResumeBGVideo();
            SoundManager.Instance.RoundMusic.Play();

            //resume running out sound
            if (this.timeIsRunningOut)
                SoundManager.Instance.RunningOutSound.Play();
        }

        /// <summary>
        /// Startet das Level von vorne und schliesst das Pausemenue
        /// </summary>
        public void Restart()
        {
            this.levelTimerVal = TimeSpan.FromSeconds(this.currentLevel.LevelDuration);
            this.timeIsRunningOut = false;
            ProtonManager.Instance.ResetProtons();
            SoundManager.Instance.ResetRoundMusic();
            ResetScore();
            Resume();
        }

        /// <summary>
        /// Laedt das gegebene Level. 
        /// Initialisiert die Spielvariablen in abhaehngigkeit zum Level und startet dieses.
        /// </summary>
        public void LoadLevel(Level level)
        {
            this.currentLevel = level;
            levelTimerVal = TimeSpan.FromSeconds(level.LevelDuration);
            levelTimer.Start();
            ResetScore();
            ProtonManager.Instance.DestroyProtons();
            ProtonManager.Instance.StartDetection();
            ProtonManager.Instance.ShowOneProtonPerPlayer();

            MainWindow.Instance.ResumeBGVideo();
        }

        /// <summary>
        /// Beendet das aktuelle Level ordnungsgemaess und zeigt wechselt zum EnterHighscoreScreen
        /// </summary>
        public void EndLevel()
        {
            levelTimer.Stop();
            ViewManager.Instance.EnterScoreMenu.UpdateScoreValue(currentLevel.CurrentScore);
            ViewManager.Instance.EnterScoreMenu.UpdateHighScoreSnippet(currentLevel.CurrentScore);
            ViewManager.Instance.ChangeView(ViewManager.Views.EnterScoreMenu);
            ProtonManager.Instance.DestroyProtons();
            ProtonManager.Instance.StopDetection();
            ProtonManager.Instance.HideProtons();
            MainWindow.Instance.PauseBGVideo();
            SoundManager.Instance.RunningOutSound.Stop();
            SoundManager.Instance.PlayBuzzerSound();
        }

        /// <summary>
        /// Erzeugt einen Highscoreeintrag in der HighscoreXML unter gegebenen Spielernamen, sofern saveHighScore = true.
        /// Wechselt anschliessend zum Hauptmenue.
        /// </summary>
        public void ExitLevel(bool saveHighScore, string playername)
        {
            if (saveHighScore)
                SaveManager.Instance.InsertHighScoreEntry(new HighScoreEntry(playername, currentLevel.CurrentScore));

            currentLevel = null;
            ViewManager.Instance.ChangeView(ViewManager.Views.MainMenu);
        }

        /// <summary>
        /// Fuegt dem aktuellen Level den deltaWert als Punktzahl hinzu und aktualisiert deren Ausgabe entsprechend
        /// </summary>
        /// <param name="delta"></param>
        public void AddScore(int delta)
        {
            currentLevel.CurrentScore += delta;
            ViewManager.Instance.GameView.UpdateScoreValue(currentLevel.CurrentScore);
        }

        /// <summary>
        /// Setzt die Punktzahl des aktuellen Levels auf 0 und aktualisiert deren Ausgabe entsprechend
        /// </summary>
        private void ResetScore()
        {
            currentLevel.CurrentScore = 0;
            ViewManager.Instance.GameView.UpdateScoreValue(currentLevel.CurrentScore);
        }

    }
}
