﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Infotable.view;
using Microsoft.Surface.Presentation.Controls;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows;
using System.Windows.Media.Imaging;
using Infotable.model;
using System.Diagnostics;
using System.IO;
using Infotable.service.log;
using Infotable.view.usercontrol;

namespace Infotable
{
    /// <summary>
    /// Registers on Person property to show the ProtonColliderButton if the chosen Person is Peter Higgs.
    /// If the button will be clicked then the application "Proton Collider" will be started.
    /// The application is located in PATH_TO_PROTON_COLLIDER_EXE. If the application does not exist ther,
    /// one has to copy it to this location.
    /// </summary>
    class ProtonColliderLoader
    {
        private static ProtonColliderLoader instance;
        public static ProtonColliderLoader Instance
        {
            get
            {
                if (null == instance)
                    instance = new ProtonColliderLoader();

                return instance;
            }
            private set{}
        }

        private const double HEIGHT = 100;
        private const double WIDTH = 100;
        private const string PATH_TO_PROTON_COLLIDER_EXE = @"C:\InfoTable\ProtonCollider\ProtonCollider.exe";
        private FluidKit.Controls.ElementFlow persons;
        private bool protonColliderFileExists;

        public SurfaceButton ProtonColliderStarterButton
        {
            private set;
            get;
        }

        public Label ProtonColliderStarterLabel
        {
            private set;
            get;
        }

        public FluidKit.Controls.ElementFlow Persons
        {
            get { return this.persons; }
            set
            {
                this.persons = value; 
                Person selectedPerson = this.persons.SelectedItem as Person;
                SetVisibilityDependingOn(selectedPerson);
            }
        }

        private ProtonColliderLoader()
        {
            ProtonColliderStarterButton = GetButtonWhichStartsProtonCollider();
            ProtonColliderStarterLabel = new Label();
            ProtonColliderStarterLabel.Content = "Spiel starten";
            ProtonColliderStarterLabel.Foreground = new SolidColorBrush(Colors.White);
            ProtonColliderStarterLabel.Margin = new Thickness(0, -10, 0, 0);

            // Check if ProtonCollider-application exists
            protonColliderFileExists = File.Exists(PATH_TO_PROTON_COLLIDER_EXE);
            if (!protonColliderFileExists)
            {
                LogWriter.writeLog("Could not find file: " + PATH_TO_PROTON_COLLIDER_EXE);
            }
        }


        private SurfaceButton GetButtonWhichStartsProtonCollider()
        {
            SurfaceButton protonColliderButton = new SurfaceButton();
            Image proColLogo = new Image();
            proColLogo.Source = new BitmapImage(new Uri("pack://application:,,,/icon/proton_collider_logo.png", UriKind.Absolute));
            protonColliderButton.Content = proColLogo;
            protonColliderButton.Background = Brushes.Transparent;
            protonColliderButton.Padding = new Thickness(5);
            protonColliderButton.Click += (s, args) =>
            {
                LaunchProtonCollider();
            };
            return protonColliderButton;
        }

        public void Persons_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // If the selected person is Peter Higgs, show the Button to start ProtonCollider, otherwise hide it.
            Person selectedPerson = persons.SelectedItem as Person;
            SetVisibilityDependingOn(selectedPerson);
        }

        private void SetVisibilityDependingOn(Person selectedPerson)
        {
            if (null != selectedPerson && selectedPerson.Nachname == "Higgs" && protonColliderFileExists)
            {
                ProtonColliderStarterButton.Visibility = Visibility.Visible;
                ProtonColliderStarterLabel.Visibility = Visibility.Visible;
            }
            else
            {
                ProtonColliderStarterButton.Visibility = Visibility.Hidden;
                ProtonColliderStarterLabel.Visibility = Visibility.Hidden;
            }
        }

        private void LaunchProtonCollider()
        {
            if (!protonColliderFileExists)
                return;

            // Prepare the process to run
            ProcessStartInfo start = new ProcessStartInfo();
            // Enter the executable to run, including the complete path
            start.FileName = PATH_TO_PROTON_COLLIDER_EXE;            

            // Run the external process & wait for it to finish
            using (Process proc = Process.Start(start))
            {
                proc.WaitForExit();
            }
        }
    }
}
