﻿using Infotable_Admin.model;
using Infotable_Admin.model.enums;
using Infotable_Admin.mvvm.commanding;
using Infotable_Admin.mvvm.locator;
using Infotable_Admin.mvvm.viewmodel;
using Infotable_Admin.service;
using Infotable_Admin.service.log;
using Microsoft.Win32;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Input;

namespace Infotable_Admin.viewmodel
{
    [LocatorAttribute(ViewModelNames.MAIN_VIEW_NAME)]
    public class MainViewModel : ViewModelBase
    {
        #region Members

        // aktuelle Personen
        private ObservableCollection<Person> personen;
        private Person person;
        private BackgroundWorker bgWorkerLadePersonen;

        // aktuelle Bilder der selektierten Person
        private ObservableCollection<Datei> bilder;
        private Datei bild;

        // aktuelle Videos der selektierten Person
        private ObservableCollection<Datei> videos;
        private Datei video;

        // alle News
        private ObservableCollection<News> neuigkeiten;
        private News neuigkeit;
        private BackgroundWorker bgWorkerLadeNeuigkeiten;

        // welches UserControl soll angezeigt werden
        private Visibility showPerson;
        private Visibility showNeuigkeit;

        // 
        private bool isTreeViewPersonSelected;
        private bool isTreeViewNewsSelected;

        #endregion

        #region Constructor

        public MainViewModel()
        {
            XML.SetDirectory();

            this.personen = new ObservableCollection<Person>();
            this.person = null;
            this.bilder = new ObservableCollection<Datei>();
            this.bild = null;
            this.videos = new ObservableCollection<Datei>();
            this.video = null;
            this.Neuigkeiten = new ObservableCollection<News>();
            this.Neuigkeit = new News();

            this.bgWorkerLadePersonen = new BackgroundWorker();
            this.bgWorkerLadePersonen.DoWork += bgWorkerLadePersonen_DoWork;
            this.bgWorkerLadePersonen.RunWorkerAsync();

            this.bgWorkerLadeNeuigkeiten = new BackgroundWorker();
            this.bgWorkerLadeNeuigkeiten.DoWork += bgWorkerLadeNeuigkeiten_DoWork;
            this.bgWorkerLadeNeuigkeiten.RunWorkerAsync();

            this.showPerson = Visibility.Collapsed;
            this.showNeuigkeit = Visibility.Collapsed;

            this.isTreeViewPersonSelected = false;
            this.isTreeViewNewsSelected = false;
        }

        #endregion

        #region Properties

        public ObservableCollection<Person> Personen
        {
            get { return personen; }
            set
            {
                if (personen == value)
                    return;
                personen = value;
                RaisePropertyChanged("Personen");
            }
        }

        public Person Person
        {
            get { return person; }
            set
            {
                if (person == value)
                    return;
                person = value;
                RaisePropertyChanged("Person");

                if (value != null)
                {
                    // setze aktuelle Bilder der Person
                    this.Bilder = value.BilderPfade;
                    // setze aktuelle Videos der Person
                    this.Videos = value.VideoPfade;
                    // zeige Personen UserControl an wenn eine Person markiert wird
                    if (this.ShowPerson == Visibility.Collapsed)
                    {
                        this.ShowPerson = Visibility.Visible;
                        this.ShowNeuigkeit = Visibility.Collapsed;

                        // setze selectedItem von Neuigkeit auf null
                        this.Neuigkeit = null;
                    }

                    // deselektiere TreeViewItems
                    this.IsTreeViewPersonSelected = false;
                    this.IsTreeViewNewsSelected = false;
                }
                else
                    this.ShowPerson = Visibility.Collapsed;
            }
        }

        public ObservableCollection<Datei> Bilder
        {
            get { return bilder; }
            set
            {
                if (bilder == value)
                    return;
                bilder = value;
                RaisePropertyChanged("Bilder");
            }
        }

        public Datei Bild
        {
            get { return bild; }
            set
            {
                if (bild == value)
                    return;
                bild = value;
                RaisePropertyChanged("Bild");
            }
        }

        public ObservableCollection<Datei> Videos
        {
            get { return videos; }
            set
            {
                if (videos == value)
                    return;
                videos = value;
                RaisePropertyChanged("Videos");
            }
        }

        public Datei Video
        {
            get { return video; }
            set
            {
                if (video == value)
                    return;
                video = value;
                RaisePropertyChanged("Video");
            }
        }

        public ObservableCollection<News> Neuigkeiten
        {
            get { return neuigkeiten; }
            set
            {
                if (neuigkeiten == value)
                    return;
                neuigkeiten = value;
                RaisePropertyChanged("Neuigkeiten");
            }
        }

        public News Neuigkeit
        {
            get { return neuigkeit; }
            set
            {
                if (neuigkeit == value)
                    return;
                neuigkeit = value;
                RaisePropertyChanged("Neuigkeit");

                if (value != null)
                {
                    // zeige Neuigkeit UserControl an wenn eine Neuigkeit markiert wird
                    if (this.ShowNeuigkeit == Visibility.Collapsed)
                    {
                        this.ShowNeuigkeit = Visibility.Visible;
                        this.ShowPerson = Visibility.Collapsed;

                        // setze selectedItem von Person auf null
                        this.Person = null;
                    }

                    // deselektiere TreeViewItems
                    this.IsTreeViewPersonSelected = false;
                    this.IsTreeViewNewsSelected = false;
                }
                else
                    this.ShowNeuigkeit = Visibility.Collapsed;
            }
        }

        public Visibility ShowPerson
        {
            get { return showPerson; }
            set
            {
                if (showPerson == value)
                    return;
                showPerson = value;
                RaisePropertyChanged("ShowPerson");
            }
        }

        public Visibility ShowNeuigkeit
        {
            get { return showNeuigkeit; }
            set
            {
                if (showNeuigkeit == value)
                    return;
                showNeuigkeit = value;
                RaisePropertyChanged("ShowNeuigkeit");
            }
        }

        public bool IsTreeViewPersonSelected
        {
            get { return isTreeViewPersonSelected; }
            set
            {
                if (isTreeViewPersonSelected == value)
                    return;
                isTreeViewPersonSelected = value;
                RaisePropertyChanged("IsTreeViewPersonSelected");

                if (value)
                    this.Clear();
            }
        }

        public bool IsTreeViewNewsSelected
        {
            get { return isTreeViewNewsSelected; }
            set
            {
                if (isTreeViewNewsSelected == value)
                    return;
                isTreeViewNewsSelected = value;
                RaisePropertyChanged("IsTreeViewNewsSelected");

                if (value)
                    this.Clear();
            }
        }

        #endregion

        #region Commands

        #region RefreshCommand

        private RelayCommand refreshCommand;

        public ICommand RefreshCommand
        {
            get
            {
                if (this.refreshCommand == null)
                    this.refreshCommand = new RelayCommand(Refresh);
                return this.refreshCommand;
            }
        }

        private void Refresh()
        {
            this.bgWorkerLadePersonen.RunWorkerAsync();
            this.bgWorkerLadeNeuigkeiten.RunWorkerAsync();
        }

        #endregion

        #region AddCommand

        private RelayCommand addCommand;

        public ICommand AddCommand
        {
            get
            {
                if (this.addCommand == null)
                    this.addCommand = new RelayCommand(Add);
                return this.addCommand;
            }
        }

        private void Add()
        {
            if (this.IsTreeViewPersonSelected || this.Person != null)
            {
                Person person = new Person();
                    // hole ID
                    int id = 0;
                    for (int i = 0; i < this.Personen.Count; i++)
                    {
                        if (this.Personen[i].Id > id)
                            id = this.Personen[i].Id;
                    }
                    id++;
                    // erzeuge neues verzeichnis
                    string pfad = XML.medienVerzeichnis + id + "\\";
                    if (!Directory.Exists(pfad))
                        Directory.CreateDirectory(pfad);
                person.Id = id;
                person.SetName();
                person.Change = true;
                person.Verzeichnis = pfad;

                // füge in XML hinzu
                XML.addElement(person.Id, model.enums.Element.Person);

                this.Personen.Add(person);
                this.Person = person;
            }
            else if (this.IsTreeViewNewsSelected || this.Neuigkeit != null)
            {
                News neuigkeit = new News();
                    int id = 0;
                    for (int i = 0; i < this.Neuigkeiten.Count; i++)
                    {
                        if (this.Neuigkeiten[i].Id > id)
                            id = this.Neuigkeiten[i].Id;
                    }
                neuigkeit.Id = id + 1;
                neuigkeit.Change = true;

                XML.addElement(neuigkeit.Id, model.enums.Element.Neuigkeit);

                this.Neuigkeiten.Add(neuigkeit);
                this.Neuigkeit = neuigkeit;
            }
        }

        #endregion

        #region RemoveCommand

        private RelayCommand removeCommand;

        public ICommand RemoveCommand
        {
            get
            {
                if (this.removeCommand == null)
                    this.removeCommand = new RelayCommand(Remove);
                return this.removeCommand;
            }
        }

        private void Remove()
        {
            if (this.Person != null)
            {
                MessageBoxResult result = MessageBox.Show("Soll \'" + this.Person.Name + "\' wirklich gelöscht werden?", "Person löschen", MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (result == MessageBoxResult.Yes)
                {
                    string verzeichnis = this.Person.Verzeichnis;

                    // lösche News aus XML-Datei
                    XML.removeElement(this.Person.Id, model.enums.Element.Person);

                    // lösche person aus Liste
                    this.Personen.Remove(this.Person);

                    // setze alles auf null
                    this.Clear();

                    // lösche verzeichnis
                    RemoveDirectory(verzeichnis);
                }
            }
            else if (this.Neuigkeit != null)
            {
                MessageBoxResult result = MessageBox.Show("Soll \'" + this.Neuigkeit.Titel + "\' wirklich gelöscht werden?", "News löschen", MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (result == MessageBoxResult.Yes)
                {
                    // lösche News aus XML-Datei
                    XML.removeElement(this.Neuigkeit.Id, model.enums.Element.Neuigkeit);

                    // lösche News aus Liste
                    this.Neuigkeiten.Remove(this.Neuigkeit);

                    // setze alles auf null
                    this.Clear();
                }
            }
        }

        #endregion

        #region AddPictureCommand

        private RelayCommand addPictureCommand;

        public ICommand AddPictureCommand
        {
            get
            {
                if (this.addPictureCommand == null)
                    this.addPictureCommand = new RelayCommand(AddPicture);
                return this.addPictureCommand;
            }
        }

        private void AddPicture()
        {
            if (this.Person == null)
                return;

            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = true;

            Nullable<bool> result = ofd.ShowDialog();

            if (result == true)
            {
                for (int i = 0; i < ofd.FileNames.Length; i++)
                {
                    string sourceFileName = ofd.FileNames[i];
                    string name = new DirectoryInfo(sourceFileName).Name;
                    string destFileName = this.Person.Verzeichnis + name;
                    // kopiere Bild in das Personen-Dateisystem
                    Exception ex = CopyData(sourceFileName, destFileName);
                    if (ex != null)
                        continue;

                    Datei element = new Datei(destFileName, new DirectoryInfo(destFileName).Parent.FullName, name, Data.Bild);

                    this.Bilder.Add(element);

                    XML.addDatei(this.Person.Id, "bild", element.Name);

                    this.Bild = element;
                }
            }
        }

        #endregion

        #region RemovePictureCommand

        private RelayCommand removePictureCommand;

        public ICommand RemovePictureCommand
        {
            get
            {
                if (this.removePictureCommand == null)
                    this.removePictureCommand = new RelayCommand(RemovePicture);
                return this.removePictureCommand;
            }
        }

        private void RemovePicture()
        {
            if (this.Bild == null)
                return;

            MessageBoxResult result = MessageBox.Show("Soll das ausgewählte Bild wirklich gelöscht werden?", "Bild löschen", MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (result == MessageBoxResult.Yes)
            {
                Datei temp = new Datei(this.Bild);

                // lösche Bild aus Liste
                this.Bilder.Remove(this.Bild);

                // setze "selected" Bild auf null
                this.Bild = null;

                // lösche in XML
                XML.removeDatei(this.Person.Id, "bild", temp.Name);

                // lösche Bild aus Verzeichnis
                RemoveData(temp.Pfad);
            }
        }

        #endregion

        #region AddVideoCommand

        private RelayCommand addVideoCommand;

        public ICommand AddVideoCommand
        {
            get
            {
                if (this.addVideoCommand == null)
                    this.addVideoCommand = new RelayCommand(AddVideo);
                return this.addVideoCommand;
            }
        }

        private void AddVideo()
        {
            if (this.Person == null)
                return;

            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = true;

            Nullable<bool> result = ofd.ShowDialog();

            if (result == true)
            {
                for (int i = 0; i < ofd.FileNames.Length; i++)
                {
                    string sourceFileName = ofd.FileNames[i];
                    string name = new DirectoryInfo(sourceFileName).Name;
                    string destFileName = this.Person.Verzeichnis + name;
                    // kopiere Video in das Personen-Dateisystem
                    Exception ex = CopyData(sourceFileName, destFileName);
                    if (ex != null)
                        continue;

                    Datei element = new Datei(destFileName, new DirectoryInfo(destFileName).Parent.FullName, name, Data.Video);

                    this.Videos.Add(element);

                    XML.addDatei(this.Person.Id, "video", element.Name);

                    this.Video = element;
                }
            }
        }

        #endregion

        #region RemoveVideoCommand

        private RelayCommand removeVideoCommand;

        public ICommand RemoveVideoCommand
        {
            get
            {
                if (this.removeVideoCommand == null)
                    this.removeVideoCommand = new RelayCommand(RemoveVideo);
                return this.removeVideoCommand;
            }
        }

        private void RemoveVideo()
        {
            if (this.Video == null)
                return;

            MessageBoxResult result = MessageBox.Show("Soll das ausgewählte Video wirklich gelöscht werden?", "Video löschen", MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (result == MessageBoxResult.Yes)
            {
                Datei temp = new Datei(this.Video);

                // lösche Video aus Liste
                this.Videos.Remove(this.Video);

                // setze "selected" Video auf null
                this.Video = null;

                // lösche in XML
                XML.removeDatei(this.Person.Id, "video", temp.Name);
            
                // lösche Video aus Verzeichnis
                RemoveData(temp.Pfad);
            }
        }

        #endregion

        #endregion

        #region Events

        void bgWorkerLadePersonen_DoWork(object sender, DoWorkEventArgs e)
        {
            this.Personen = XML.getPersonen();

            this.Clear();
        }

        void bgWorkerLadeNeuigkeiten_DoWork(object sender, DoWorkEventArgs e)
        {
            this.Neuigkeiten =  XML.getNeuigkeiten();

            this.Clear();
        }

        #endregion

        #region Methods

        public void Clear()
        {
            this.Person = null;
            this.Bilder = new ObservableCollection<Datei>();
            this.Bild = null;
            this.Videos = new ObservableCollection<Datei>();
            this.Video = null;
            this.Neuigkeit = null;
        }

        public Exception CopyData(string sourceFileName, string destFileName)
        {
            Exception result = null;

            try
            {
                File.Copy(sourceFileName, destFileName, true);
            }
            catch (Exception ex)
            {
                result = ex;
                LogWriter.writeLog(ex);
            }

            return result;
        }

        public static Exception RemoveData(string path)
        {
            Exception result = null;

            try
            {
                File.Delete(path);
            }
            catch (Exception ex)
            {
                result = ex;
                LogWriter.writeLog(ex);
            }

            return result;
        }

        public static Exception RemoveDirectory(string path)
        {
            Exception result = null;

            try
            {
                if (Directory.Exists(path))
                {
                    string[] files = Directory.GetFiles(path);
                    for (int i = 0; i < files.Length; i++)
                    {
                        RemoveData(files[i]);
                    }

                    Directory.Delete(path, true);
                }
            }
            catch (Exception ex)
            {
                result = ex;
                LogWriter.writeLog(ex);
            }

            return result;
        }

        #endregion
    }
}
