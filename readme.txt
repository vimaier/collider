A WPF based application which represents a cooperative multiplayer game that allows to demonstrate the collision of particles comparable to the experiments at CERN.

**The project was part of our studies in Computer Science (M.Sc.).**

Der Abgabeordner entspricht unserem git Repository und enthält:

* \.git: Unsichtbarer Ordner mit git-spezifischen Files zum Repository

* \Anwendung: Enthält die erstellten, ausführbaren Anwendungen inklusive zum Start benötigter Ressourcen

* \Docs: Enthält Projektpräsentation, Projektdokumentation, Konzeptpapier und Diagrammdateien

* \Higgs-Beschreibung: Bilder, Videos und Text zur Higgs-Persönlichkeit

* \infotable: Projektdateien zum Infotable (Adminversion und Standardversion)

* \project: Projektdateien zum Proton Collider

* \ProtonCollider: Anwendung zum Kopieren nach C://Infotafel/ (siehe Dokumentation)

* \tmp: Projektdateien eines Testprojektes mit dem die Grundfunktionalitäten der Protonen entwickelt wurde

* .gitignore: git-spezifische Ignore-Datei

* readme.txt: Diese Datei