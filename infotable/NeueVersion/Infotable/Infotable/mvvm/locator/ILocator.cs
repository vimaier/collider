﻿
namespace Infotable.mvvm.locator
{
    public interface ILocator
    {
        void Register(string name, object o);
        void Reset();
        object GetInstance(string name);
        object this[string name] { get; }
    }
}
