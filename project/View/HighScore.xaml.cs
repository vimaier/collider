﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ProtonCollider.Model;
using ProtonCollider.Controller;
using System.Collections.ObjectModel;
using System.Globalization;

namespace ProtonCollider.View
{
    /// <summary>
    /// Interaktionslogik für HighScore.xaml
    /// </summary>
    public partial class HighScore : UserControl
    {
        /// <summary>
        /// Konstruktor des Views
        /// </summary>
        public HighScore()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Wechselt die View zum Hauptmenue
        /// </summary>
        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            ViewManager.Instance.ChangeView(ViewManager.Views.MainMenu);
        }

        /// <summary>
        /// Gibt die uebergebene Highscoreliste auf der Highscoretabelle aus
        /// </summary>
        public void UpdateHighScoreList(ReadOnlyCollection<HighScoreEntry> highScore)
        {
            HighScoreListView.Items.Clear();
            foreach (HighScoreEntry item in highScore)
            {
                HighScoreListView.Items.Add(item);
            }
        }

        /// <summary>
        /// Passt die Spaltenbreiten der Highscoretabelle prozentual an
        /// </summary>
        private void CalcColumnWidths(object sender, SizeChangedEventArgs e)
        {

            ListView listView = sender as ListView;
            GridView gView = listView.View as GridView;

            // 100% width of listview
            var workingWidth = listView.ActualWidth - SystemParameters.VerticalScrollBarWidth; // take into account vertical scrollbar
            
            // width of columns in percentage (100% in sum, tested values) 
            var col1 = 0.1;
            var col2 = 0.495;
            var col3 = 0.495;

            gView.Columns[0].Width = workingWidth*col1;
            gView.Columns[1].Width = workingWidth*col2;
            gView.Columns[2].Width = workingWidth*col3;
        }
        
    }
}
