﻿using Infotable_Admin.mvvm.model;
using Infotable_Admin.service;
using System;
using System.Collections.ObjectModel;

namespace Infotable_Admin.model
{
    public class Person : ModelBase, IComparable
    {
        #region Members

        private int id;
        private string nachname;
        private string vorname;
        private DateTime geburtsdatum;
        private DateTime sterbedatum;
        private string leben;
        private string werdegang;
        private ObservableCollection<Datei> bilderPfade;
        private ObservableCollection<Datei> videoPfade;

        // displayMemberPath in ListView
        private string name;

        // der Pfad indem sich Bilder und Videos befinden
        private string verzeichnis;

        // Ist dieses Attribut auf "true" gesetzt, werden Properties beim Neusetzen in der XML-Datei gespeichert
        // Ist dieses Attribut auf "false" gesetzt, so werden keine Änderungen an der XML-Datei vorgenommen.
        private bool change;
        
        #endregion

        #region Constructors
        public Person()
        {
            this.Clear();
        }

        #endregion

        #region Properties

        public int Id
        {
            get { return id; }
            set
            {
                if (id == value)
                    return;
                id = value;
                RaisePropertyChanged("Id");
            }
        }

        public string Nachname
        {
            get { return nachname; }
            set
            {
                if (nachname == value)
                    return;
                nachname = value;
                RaisePropertyChanged("Nachname");

                this.SetName();

                if (this.Change)
                {
                    XML.editValue(this.id, enums.Element.Person, "nachname", value);
                }
            }
        }

        public string Vorname
        {
            get { return vorname; }
            set
            {
                if (vorname == value)
                    return;
                vorname = value;
                RaisePropertyChanged("Vorname");

                this.SetName();

                if (this.Change)
                {
                    XML.editValue(this.id, enums.Element.Person, "vorname", value);
                }
            }
        }

        public DateTime Geburtsdatum
        {
            get { return geburtsdatum; }
            set
            {
                if (geburtsdatum == value)
                    return;
                geburtsdatum = value;
                RaisePropertyChanged("Geburtsdatum");

                if (this.Change)
                {
                    XML.editValue(this.id, enums.Element.Person, "geburtsdatum", value.ToShortDateString());
                }
            }
        }

        public DateTime Sterbedatum
        {
            get { return sterbedatum; }
            set
            {
                if (sterbedatum == value)
                    return;
                sterbedatum = value;
                RaisePropertyChanged("Sterbedatum");

                if (this.Change)
                {
                    XML.editValue(this.id, enums.Element.Person, "sterbedatum", value.ToShortDateString());
                }
            }
        }

        public string Leben
        {
            get { return leben; }
            set
            {
                if (leben == value)
                    return;
                leben = value;
                RaisePropertyChanged("Leben");

                if (this.Change)
                {
                    XML.editValue(this.id, enums.Element.Person, "leben", value);
                }
            }
        }

        public string Werdegang
        {
            get { return werdegang; }
            set
            {
                if (werdegang == value)
                    return;
                werdegang = value;
                RaisePropertyChanged("Werdegang");

                if (this.Change)
                {
                    XML.editValue(this.id, enums.Element.Person, "werdegang", value);
                }
            }
        }

        public ObservableCollection<Datei> BilderPfade
        {
            get { return bilderPfade; }
            set
            {
                if (bilderPfade == value)
                    return;
                bilderPfade = value;
                RaisePropertyChanged("BilderPfade");
            }
        }

        public ObservableCollection<Datei> VideoPfade
        {
            get { return videoPfade; }
            set
            {
                if (videoPfade == value)
                    return;
                videoPfade = value;
                RaisePropertyChanged("VideoPfade");
            }
        }

        public string Name
        {
            get { return name; }
            set
            {
                if (name == value)
                    return;
                name = value;
                RaisePropertyChanged("Name");
            }
        }

        public string Verzeichnis
        {
            get { return verzeichnis; }
            set
            {
                if (verzeichnis == value)
                    return;
                verzeichnis = value;
                RaisePropertyChanged("Verzeichnis");
            }
        }

        public bool Change
        {
            get { return change; }
            set
            {
                if (change == value)
                    return;
                change = value;
            }
        }

        #endregion

        #region Methods

        public void Clear()
        {
            this.id = 0;
            this.nachname = string.Empty;
            this.vorname = string.Empty;
            this.geburtsdatum = new DateTime();
            this.sterbedatum = new DateTime();
            this.leben = string.Empty;
            this.werdegang = string.Empty;
            this.bilderPfade = new ObservableCollection<Datei>();
            this.videoPfade = new ObservableCollection<Datei>();
            this.name = string.Empty;
            this.verzeichnis = string.Empty;
            this.change = false;
        }

        public void SetName()
        {
            if (this.vorname.Equals(string.Empty) && this.nachname.Equals(string.Empty))
                this.Name = "empty";
            else if (this.vorname.Equals(string.Empty))
                this.Name = this.nachname;
            else if (this.nachname.Equals(string.Empty))
                this.Name = this.vorname;
            else
                this.Name = this.nachname + ", " + this.vorname;
        }

        public int CompareTo(object o)
        {
            Person p = (Person)o;
            return String.Compare(this.Name, p.Name);
        }

        #endregion
    }
}
