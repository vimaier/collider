﻿using Infotable.model;
using Infotable.model.enums;
using Infotable.service.log;
using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Xml;

namespace Infotable.service
{
    /// <summary>
    /// Ist für die Kommunikation zwischen XML Dateien und Programm zuständig.
    /// </summary>
    public static class XML
    {
        private static string configDatei;
        public static string verzeichnis;
        private static string personenDatei;
        private static string newsDatei;
        public static string medienVerzeichnis;

        public static int updatePersonTime;
        public static int updateNewsTime;
        public static int showNewsTime;

        /// <summary>
        /// setzt das Verzeichnis sowie die Personen- und News-Xml-Datei
        /// </summary>
        public static void SetDirectory()
        {
            configDatei = AppDomain.CurrentDomain.BaseDirectory + "options.xml";

            XmlDocument doc = new XmlDocument();
            try
            {
                doc.Load(configDatei);
                XmlElement root = doc.DocumentElement;

                XmlNodeList configSettings = root.ChildNodes;

                for (int i = 0; i < configSettings.Count; i++)
                {
                    XmlNode attribut = configSettings.Item(i);

                    switch (attribut.Name)
                    {
                        case "server": verzeichnis = attribut.InnerText + "\\"; break;
                        case "UpdatePersonXmlStunden": updatePersonTime = Convert.ToInt32(attribut.InnerText); break;
                        case "UpdateNewsXmlMinuten": updateNewsTime = Convert.ToInt32(attribut.InnerText); break;
                        case "ShowNewsTimeSekunden": showNewsTime = Convert.ToInt32(attribut.InnerText); break;
                        default: break;
                    }
                }
            }
            catch (Exception ex)
            {
                LogWriter.writeLog(ex);

                verzeichnis = "C:\\Infotable\\";
                updatePersonTime = 4;
                updateNewsTime = 15;
                showNewsTime = 2;
            }

            personenDatei = verzeichnis + "personen.xml";
            medienVerzeichnis = verzeichnis + "medien\\";
            newsDatei = verzeichnis + "news.xml";
        }

        /// <summary>
        /// liest alle Personen aus einem Dateisystem, bestehend aus XML Dateien aus.
        /// Inklusive der Bilder und Videos.
        /// </summary>
        /// <returns>die Liste aller Personen als ObservableCollection</returns>
        public static ObservableCollection<Person> getPersonen()
        {
            ArrayList personen = new ArrayList();

            XmlDocument doc = new XmlDocument();
            doc.Load(personenDatei);
            XmlElement root = doc.DocumentElement;

            XmlNodeList personenListe = root.ChildNodes;

            for (int i = 0; i < personenListe.Count; i++)
            {
                XmlNode personKnoten = personenListe.Item(i);

                Person person = new Person();
                person.Id = Convert.ToInt32(personKnoten.Attributes.GetNamedItem("id").InnerText);
                person.Verzeichnis = medienVerzeichnis + person.Id + "\\";

                XmlNodeList attribute = personKnoten.ChildNodes;

                for (int j = 0; j < attribute.Count; j++)
                {
                    XmlNode attribut = attribute.Item(j);

                    switch (attribut.Name)
                    {
                        case "vorname": person.Vorname = attribut.InnerText; break;
                        case "nachname": person.Nachname = attribut.InnerText; break;
                        case "geburtsdatum":
                            {
                                string[] gebdatumString = attribut.InnerText.Split('.');
                                if (gebdatumString.Length > 1)
                                    person.Geburtsdatum = new DateTime(Convert.ToInt32(gebdatumString[2]), Convert.ToInt32(gebdatumString[1]), Convert.ToInt32(gebdatumString[0]));
                                break;
                            }
                        case "sterbedatum":
                            {
                                string[] stedatumString = attribut.InnerText.Split('.');
                                if (stedatumString.Length > 1)
                                    person.Sterbedatum = new DateTime(Convert.ToInt32(stedatumString[2]), Convert.ToInt32(stedatumString[1]), Convert.ToInt32(stedatumString[0]));
                                break;
                            }
                        case "leben": person.Leben = attribut.InnerText; break;
                        case "werdegang": person.Werdegang = attribut.InnerText; break;
                        case "video":
                            {
                                person.VideoPfade.Add(new Datei(medienVerzeichnis + person.Id + "\\" + attribut.InnerText, medienVerzeichnis + person.Id, attribut.InnerText, Data.Video));
                                break;
                            }
                        case "bild":
                            {
                                person.BilderPfade.Add(new Datei(medienVerzeichnis + person.Id + "\\" + attribut.InnerText, medienVerzeichnis + person.Id, attribut.InnerText, Data.Bild));
                                break;
                            }
                        default: break;
                    }
                }

                // setze den anzuzeigenden Namen
                person.SetName();

                // setze Profilbild
                if (person.BilderPfade.Count > 0)
                    person.ProfilBild = person.BilderPfade[0];

                // füge Bilder und Videos in die Propertie "MediaPfade"
                person.SetMediaPfade();

                personen.Add(person);
            }

            // sortiere Liste
            personen.Sort();

            ObservableCollection<Person> result = new ObservableCollection<Person>();
            for (int i = 0; i < personen.Count; i++)
            {
                result.Add((Person)personen[i]);
            }

            return result;
        }

        /// <summary>
        /// Liest alle News aus der XML-Datei news.xml aus.
        /// </summary>
        /// <returns>Die Liste aller News als ObservableCollection</returns>
        public static ObservableCollection<News> getNeuigkeiten()
        {
            ArrayList neuigkeiten = new ArrayList();

            XmlDocument doc = new XmlDocument();
            doc.Load(newsDatei);
            XmlElement root = doc.DocumentElement;

            XmlNodeList artikelList = root.ChildNodes;

            for (int i = 0; i < artikelList.Count; i++)
            {
                XmlNode artikel = artikelList.Item(i);

                News neuigkeit = new News();
                neuigkeit.Id = Convert.ToInt32(artikel.Attributes.GetNamedItem("id").InnerText);

                XmlNodeList childNodes = artikel.ChildNodes;

                for (int j = 0; j < childNodes.Count; j++)
                {
                    XmlNode child = childNodes.Item(j);

                    switch (child.Name)
                    {
                        case "titel": neuigkeit.Titel = child.InnerText; break;
                        case "inhalt": neuigkeit.Inhalt = child.InnerText; break;
                        default: break;
                    }
                }

                neuigkeiten.Add(neuigkeit);
            }

            // sortiere Liste
            neuigkeiten.Sort();

            ObservableCollection<News> result = new ObservableCollection<News>();
            for (int i = 0; i < neuigkeiten.Count; i++)
            {
                result.Add((News)neuigkeiten[i]);
            }

            return result;
        }
    }
}
