﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProtonCollider.Model;
using System.Xml.Serialization;
using System.IO;
using System.Collections.ObjectModel;

namespace ProtonCollider.Controller
{
    /// <summary>
    /// Diese Klasse dient der Verwaltung der Highscore.
    /// Einlesen aus XML.
    /// Speichern in XML.
    /// Sortieren nach Punktzahl.
    /// etc...
    /// </summary>
    class SaveManager
    {
        private static readonly string APP_DATA_DIR = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\ProtonCollider";

        /// <summary>
        /// Dateipfad zur highscore.xml, in der die highscoreliste abgespeichert und ausgelesen wird
        /// </summary>
        private static readonly string HIGHSCORE_PATH = APP_DATA_DIR + "\\highscore.xml";

        /// <summary>
        /// Singleton Instance
        /// </summary>
        private static SaveManager instance;
        public static SaveManager Instance
        {
            get
            {
                if (null == instance)
                    instance = new SaveManager();
                return instance;
            }

            private set { }
        }

        /// <summary>
        /// Schreibt die uebergebene highscoreliste in die highscore.xml Datei
        /// </summary>
        public static void HighScoreToXML(List<HighScoreEntry> highScore)
        {
            if(!Directory.Exists(APP_DATA_DIR))
                Directory.CreateDirectory(APP_DATA_DIR);

            XmlSerializer serializer = new XmlSerializer(typeof(List<HighScoreEntry>));
            TextWriter textWriter = new StreamWriter(HIGHSCORE_PATH);
            serializer.Serialize(textWriter, highScore);
            textWriter.Close();

            Console.WriteLine("HighScore in " + HIGHSCORE_PATH + " gespeichert.");
        }

        /// <summary>
        /// Liest die highscore.xml Datei ein und erzeugt die entsprechende Highscoreliste
        /// </summary>
        public static List<HighScoreEntry> XMLToHighScore()
        {
            // Erstelle HighScoreXML falls noch nicht vorhanden
            if (!File.Exists(HIGHSCORE_PATH))
            {
                InitHighScoreXML();
            }

            // Lade HighScore Liste aus XML File
            XmlSerializer deserializer = new XmlSerializer(typeof(List<HighScoreEntry>));
            TextReader textReader = new StreamReader(HIGHSCORE_PATH);
            List<HighScoreEntry> highScore = (List<HighScoreEntry>)deserializer.Deserialize(textReader);
            textReader.Close();

            // Sortiere Liste nach Punktzahl. (Hoechste zuerst) und aktualisere Ranking
            List<HighScoreEntry> sortedHighScore = SortAndUpdateRanks(highScore);

            return sortedHighScore;
        }

        /// <summary>
        /// Legt die Highscore.xml Datei mit Beispielwerten an
        /// </summary>
        private static void InitHighScoreXML()
        {
            List < HighScoreEntry> sampleHighScore = new List<HighScoreEntry>();
            sampleHighScore.Add(new HighScoreEntry("Kevin & Viktor", 9999999, 1));
            HighScoreToXML(sampleHighScore);
        }

        /// <summary>
        /// Sortiert die uebergebene Highscoreliste und aktualisiert die Raenge ihrer Eintraege nach dem sortieren
        /// </summary>
        /// <param name="highScore"></param>
        /// <returns></returns>
        private static List<HighScoreEntry> SortAndUpdateRanks(List<HighScoreEntry> highScore)
        {
            highScore = highScore.OrderByDescending(o => o.Score).ToList();

            for (int i = 0; i < highScore.Count; i++)
                highScore[i].Rank = i + 1;

            return highScore;
        }

        /// <summary>
        /// aktuelle Highscoreliste
        /// </summary>
        private List<HighScoreEntry> highScore;

        /// <summary>
        /// Konstruktor
        /// </summary>
        private SaveManager()
        {
            // HighScore von XML laden
            this.highScore = XMLToHighScore();
        }

        public ReadOnlyCollection<HighScoreEntry> HighScore
        {
            get { return this.highScore.AsReadOnly(); }
            private set { }
        }

        /// <summary>
        /// Fuegt den uebergebenen Eintrag der Highscoreliste sortiert hinzu und speichert ihn in der xml
        /// </summary>
        /// <param name="entry"></param>
        public void InsertHighScoreEntry(HighScoreEntry entry)
        {
            // Eintrag einfuegen und Liste nach Punktzahl (hoechste zuerst) Sortieren
            this.highScore.Add(entry);
            this.highScore = SortAndUpdateRanks(this.highScore);

            // In XML speichern
            HighScoreToXML(this.highScore);
        }

        
    }
}
