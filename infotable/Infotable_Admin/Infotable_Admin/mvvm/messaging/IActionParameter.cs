﻿
namespace Infotable_Admin.mvvm.messaging
{
    interface IActionParameter
    {
        void ExecuteWithParameter(object parameter);
    }
}
