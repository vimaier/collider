﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using Microsoft.Surface.Presentation.Controls;
using System.Windows.Media;
using System.Windows.Threading;
using ProtonCollider.Controller;
using System.Windows.Shapes;
using System.Windows.Media.Effects;

namespace ProtonCollider.Model
{

    /// <summary>
    /// Splits the screen into multiple areas. This will be used to add extra points if the collision happened
    /// in the mid of the screen or subtract points if the collision was detected at the border of the screen.
    /// Tis feature motivates the users to collide the protons in the mid of the screen.
    /// </summary>
    enum CollisionArea
    {
        mid,  // Represents the center in which a collision will get the most points
        secondLevel, // This is a "thick border" around the mid area
        thirdLevel // This area is around second Level
    };
    /// <summary>
    /// This class holds only constants and methods for the enum CollisionArea.
    /// </summary>
    static class CollisionAreaMethods
    {
        private const double PERCENTAGE_OF_SCREEN_MID = 0.25;
        private const double PERCENTAGE_OF_SCREEN_2ND = 0.50;
        private const double PERCENTAGE_OF_SCREEN_3RD = 1.0;

        /// <summary>
        /// Determines dynamically the upper left point of the given area.
        /// </summary>
        public static Point UpperLeftBorderPoint(this CollisionArea area)
        {
            double percentageOfScreen;
            switch (area)
            {
                case CollisionArea.mid:
                    percentageOfScreen = PERCENTAGE_OF_SCREEN_MID;
                    break;
                case CollisionArea.secondLevel:
                    percentageOfScreen = PERCENTAGE_OF_SCREEN_2ND;
                    break;
                case CollisionArea.thirdLevel:
                    percentageOfScreen = PERCENTAGE_OF_SCREEN_3RD;
                    break;
                default:
                    throw new NotImplementedException();  // Added a new CollisionArea
            }
            double y = (MainWindow.DESIGNHEIGHT - (MainWindow.DESIGNHEIGHT * percentageOfScreen)) / 2;
            double x = (MainWindow.DESIGNWIDTH - (MainWindow.DESIGNWIDTH * percentageOfScreen)) / 2;
            return new Point(x, y);
        }
        public static Point LowerRightBorderPoint(this CollisionArea area)
        {
            double percentageOfScreen;
            switch (area)
            {
                case CollisionArea.mid:
                    percentageOfScreen = PERCENTAGE_OF_SCREEN_MID;
                    break;
                case CollisionArea.secondLevel:
                    percentageOfScreen = PERCENTAGE_OF_SCREEN_2ND;
                    break;
                case CollisionArea.thirdLevel:
                    percentageOfScreen = PERCENTAGE_OF_SCREEN_3RD;
                    break;
                default:
                    throw new NotImplementedException();  // Added a new CollisionArea
            }
            double y = (MainWindow.DESIGNHEIGHT + (MainWindow.DESIGNHEIGHT * percentageOfScreen)) / 2;
            double x = (MainWindow.DESIGNWIDTH + (MainWindow.DESIGNWIDTH * percentageOfScreen)) / 2;
            return new Point(x, y);
        }
        public static double CollisionScoreMultiplicator(this CollisionArea area)
        {
            switch (area)
            {
                case CollisionArea.mid:
                    return 3;
                case CollisionArea.secondLevel:
                    return 1.5;
                case CollisionArea.thirdLevel:
                    return 0.75;
                default:
                    throw new NotImplementedException();  // Added a new CollisionArea? 
            }
        }
    }


    /// <summary>
    /// The ProtonCollisionManager is responsible for detecting and animating collisions.
    /// </summary>
    class ProtonCollisionManager
    {
        private ProtonManager manager;
        private ProtonCollisionDetector collisionDetector;
        private ProtonCollisionAnimator collisionAnimator;
        private DispatcherTimer detectionTimer;
        private const int CHECKING_FREQUENCY = 50;  // x per seconds [Hz]

        public ProtonCollisionManager(ProtonManager manager)
        {
            this.manager = manager;
            collisionDetector = new ProtonCollisionDetector(this, manager.PlayerOneProtons(), manager.PlayerTwoProtons());
            collisionAnimator = new ProtonCollisionAnimator(this);
            detectionTimer = new DispatcherTimer();
            detectionTimer.Interval = TimeSpan.FromSeconds(1.0 / CHECKING_FREQUENCY);  // [ms]

            detectionTimer.Tick += (s, args) =>
            {
                collisionDetector.HitTest();
                collisionDetector.DestroyProtonsOutsideOfTheScreen();

            };

        }

        public void StartDetection()
        {
            detectionTimer.Start();
        }
        public void StopDetection()
        {
            detectionTimer.Stop();
        }


        protected void DetectedCollisionBetween(Proton protonOne, Proton protonTwo, CollisionArea area, Point collisionPoint)
        {

            int collisionScore = ProtonCollisionScoreCalculator.CalculateCollisionScoreOf(protonOne, protonTwo, area);
            GameManager.Instance.AddScore(collisionScore);
            collisionAnimator.AnimateCollision(protonOne, protonTwo, area, collisionScore, collisionPoint);

            // We have to handle cases in which one of the proton is not moving. We have to create then explicitly then.
            if (protonOne.IsNotMoving())
                manager.CreateNewProton(protonOne.Player);
            protonOne.Destroy();
            if (protonTwo.IsNotMoving())
                manager.CreateNewProton(protonTwo.Player);
            protonTwo.Destroy();
        }


        /// <summary>
        /// The ProtonCollisionDetector supports the ProtonCollisionManager and detects collisions.
        /// </summary>
        class ProtonCollisionDetector
        {

            private ProtonCollisionManager collisionManager;
            private IEnumerable<Proton> protonsPlayerOne;
            private IEnumerable<Proton> protonsPlayerTwo;

            private double upperBorder;
            private double lowerBorder;
            private double leftBorder;
            private double rightBorder;

            private Point upperLeftMidArea = CollisionAreaMethods.UpperLeftBorderPoint(CollisionArea.mid);
            private Point lowerRightMidArea = CollisionAreaMethods.LowerRightBorderPoint(CollisionArea.mid);
            private Point upperLeft2ndArea = CollisionAreaMethods.UpperLeftBorderPoint(CollisionArea.secondLevel);
            private Point lowerRight2ndArea = CollisionAreaMethods.LowerRightBorderPoint(CollisionArea.secondLevel);

            public ProtonCollisionDetector(ProtonCollisionManager manager, IEnumerable<Proton> protonsPlayerOne, IEnumerable<Proton> protonsPlayerTwo)
            {
                collisionManager = manager;
                this.protonsPlayerOne = protonsPlayerOne;
                this.protonsPlayerTwo = protonsPlayerTwo;

                upperBorder = 0 - Proton.PROTON_RADIUS;
                lowerBorder = MainWindow.DESIGNWIDTH + Proton.PROTON_RADIUS;
                leftBorder = 0 - Proton.PROTON_RADIUS;
                rightBorder = MainWindow.DESIGNHEIGHT + Proton.PROTON_RADIUS;

                // Draw Collision Areas to indicate where the users should try to collide
                double width2nd = lowerRight2ndArea.X - upperLeft2ndArea.X;
                double height2nd = lowerRight2ndArea.Y - upperLeft2ndArea.Y;
                ViewManager.Instance.GameView.DrawAreaBorder(height2nd, width2nd, upperLeft2ndArea);

                double widthmid = lowerRightMidArea.X - upperLeftMidArea.X;
                double heightmid = lowerRightMidArea.Y - upperLeftMidArea.Y;
                ViewManager.Instance.GameView.DrawAreaBorder(heightmid, widthmid, upperLeftMidArea);

                ViewManager.Instance.GameView.DrawMultiplierLabel(upperLeft2ndArea.Y - 100, "1x");
                ViewManager.Instance.GameView.DrawMultiplierLabel(upperLeft2ndArea.Y, "2x");
                ViewManager.Instance.GameView.DrawMultiplierLabel(upperLeftMidArea.Y, "6x");
            }

            public void HitTest()
            {   /// Test every proton of player one with the protons of player two.
                foreach (Proton proton1 in protonsPlayerOne)
                    if (proton1.IsNotDestroyed())
                        foreach (Proton proton2 in protonsPlayerTwo)
                            if (proton2.IsNotDestroyed())
                                if (HitEachother(proton1, proton2))
                                {
                                    Point collisionPoint = GetCollisionPoint(proton1, proton2);
                                    CollisionArea area = DetectCollisionArea(collisionPoint);
                                    collisionManager.DetectedCollisionBetween(proton1, proton2, area, collisionPoint);
                                    break;
                                }
            }

            private bool HitEachother(Proton protonOne, Proton protonTwo)
            {
                // Two protons hit each other if the distance between the both centers
                // is smaller than the radius of a proton.
                // For simplicity and to save calculations we test only the distance in X and Y.
                double r = Proton.PROTON_RADIUS;

                if (Math.Abs(protonOne.ActualCenter.X - protonTwo.ActualCenter.X) < r)
                    if (Math.Abs(protonOne.ActualCenter.Y - protonTwo.ActualCenter.Y) < r)
                        return true;
                return false;
            }

            private Point GetCollisionPoint(Proton protonOne, Proton protonTwo)
            {   /// The collision point is the half way  between the protons in horizontal and vertical direction
                // x = (x_1 + x_2) / 2
                double xPos = (protonOne.ActualCenter.X + protonTwo.ActualCenter.X) / 2;
                // y = (y_1 + y_2) / 2
                double yPos = (protonOne.ActualCenter.Y + protonTwo.ActualCenter.Y) / 2;

                return new Point(xPos, yPos);
            }

            /// <summary>
            /// Determines and returns the CollisionArea in which the collision occured.
            /// </summary>
            private CollisionArea DetectCollisionArea(Point collisionPoint)
            {
                double x = collisionPoint.X;
                double y = collisionPoint.Y;

                if (upperLeftMidArea.X < x && x < lowerRightMidArea.X && upperLeftMidArea.Y < y && y < lowerRightMidArea.Y)
                {
                    return CollisionArea.mid;
                }
                else if (upperLeft2ndArea.X < x && x < lowerRight2ndArea.X && upperLeft2ndArea.Y < y && y < lowerRight2ndArea.Y)
                {
                    return CollisionArea.secondLevel;
                }
                else
                {
                    return CollisionArea.thirdLevel;
                }
            }

            internal void DestroyProtonsOutsideOfTheScreen()
            {
                DestroyProtonsOutsideOfTheScreen(protonsPlayerOne);
                DestroyProtonsOutsideOfTheScreen(protonsPlayerTwo);
            }
            private void DestroyProtonsOutsideOfTheScreen(IEnumerable<Proton> protons)
            {
                foreach (Proton proton in protons)
                {
                    if (proton.IsMoving())
                    {
                        if (proton.ActualCenter.X < upperBorder || proton.ActualCenter.X > lowerBorder ||
                            proton.ActualCenter.Y < leftBorder || proton.ActualCenter.Y > rightBorder)
                        {
                            proton.Destroy();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Responsible for the animation of the collision of two protons.
        /// </summary>
        class ProtonCollisionAnimator
        {
            private const string EXPLOSION_IMAGE_SRC = @"/Resources/explosion.png";

            //collision animation
            private ProtonCollisionManager protonCollisionManager;
            private Storyboard collisionStoryboard;
            private DoubleAnimation scaleXAnimationMidArea;
            private DoubleAnimation scaleYAnimationMidArea;
            private DoubleAnimation scaleXAnimation2ndArea;
            private DoubleAnimation scaleYAnimation2ndArea;
            private DoubleAnimation scaleXAnimation3rdArea;
            private DoubleAnimation scaleYAnimation3rdArea;
            private BitmapImage explosionImageSource;
            private DoubleAnimation fadeOutAnimation;

            //score animation
            private DoubleAnimation scaleScoreXAnimation;
            private DoubleAnimation scaleScoreYAnimation;
            private DoubleAnimation fadeScoreOutAnimation;

            public ProtonCollisionAnimator(ProtonCollisionManager protonCollisionManager)
            {
                this.protonCollisionManager = protonCollisionManager;
                PrepareAnimation();
            }

            public void AnimateCollision(Proton protonOne, Proton protonTwo, CollisionArea area, int collisionScore, Point collisionPoint)
            {
                // create explosion animation
                ScatterViewItem explosionItem = CreateAnimatedExplosionSVI(area);
                explosionItem.Center = collisionPoint; //position it to the collision point
                MainWindow.Instance.GameAreaScatterView.Items.Add(explosionItem);

                // create score text animation
                ScatterViewItem scoreItem = CreateAnimatedScoreSVI(collisionScore);
                scoreItem.Center = collisionPoint; //position it to the collision point
                MainWindow.Instance.GameAreaScatterView.Items.Add(scoreItem);

                // play explosion sound
                SoundManager.Instance.PlayCollisionSound();


                // destroy image and scoretext on animation end
                collisionStoryboard.Completed += (s, args) =>
                {
                    MainWindow.Instance.GameAreaScatterView.Items.Remove(explosionItem);
                    MainWindow.Instance.GameAreaScatterView.Items.Remove(scoreItem);
                };

                // start animation
                collisionStoryboard.Begin();
            }

            private void PrepareAnimation()
            {
                // Collision Animation
                Duration duration = new Duration(TimeSpan.FromSeconds(0.25));
                scaleXAnimationMidArea = new DoubleAnimation(50.0, 1200.0, duration);
                scaleYAnimationMidArea = new DoubleAnimation(50.0, 1200.0, duration);
                scaleXAnimation2ndArea = new DoubleAnimation(50.0, 700.0, duration);
                scaleYAnimation2ndArea = new DoubleAnimation(50.0, 700.0, duration);
                scaleXAnimation3rdArea = new DoubleAnimation(50.0, 300.0, duration);
                scaleYAnimation3rdArea = new DoubleAnimation(50.0, 300.0, duration);
                fadeOutAnimation = new DoubleAnimation(1.75, 0.0, duration);

                // Score Animation
                duration = new Duration(TimeSpan.FromSeconds(0.75));
                scaleScoreXAnimation = new DoubleAnimation(50.0, 320, duration);
                scaleScoreYAnimation = new DoubleAnimation(50.0, 180, duration);
                fadeScoreOutAnimation = new DoubleAnimation(1.75, 0.0, duration);

                // Collision Storyboard
                collisionStoryboard = new Storyboard();
                collisionStoryboard.Children.Add(fadeOutAnimation);
                collisionStoryboard.Children.Add(fadeScoreOutAnimation);
                collisionStoryboard.Children.Add(scaleScoreXAnimation);
                collisionStoryboard.Children.Add(scaleScoreYAnimation);

                // Collision Image Source
                explosionImageSource = new BitmapImage();
                explosionImageSource.BeginInit();
                explosionImageSource.UriSource = new Uri(EXPLOSION_IMAGE_SRC, UriKind.Relative);
                explosionImageSource.EndInit();
            }

            private ScatterViewItem CreateAnimatedExplosionSVI(CollisionArea area)
            {
                // create explosion image
                Image explosionImage = new Image();
                explosionImage.Source = explosionImageSource;
                explosionImage.IsHitTestVisible = false;

                // create scatterview item which serves as container of the image
                ScatterViewItem item = CreateSVIContainer();
                item.Content = explosionImage;
                item.CanMove = false;
                item.CanRotate = false;
                item.CanScale = false;
                item.IsHitTestVisible = false;

                // prepare scale of the image by scaling its container
                collisionStoryboard.Children.Remove(scaleXAnimationMidArea);
                collisionStoryboard.Children.Remove(scaleYAnimationMidArea);
                collisionStoryboard.Children.Remove(scaleXAnimation2ndArea);
                collisionStoryboard.Children.Remove(scaleYAnimation2ndArea);
                collisionStoryboard.Children.Remove(scaleXAnimation3rdArea);
                collisionStoryboard.Children.Remove(scaleYAnimation3rdArea);

                DoubleAnimation scaleXAnimation;
                DoubleAnimation scaleYAnimation;
                switch (area)
                {
                    case CollisionArea.mid:
                        scaleXAnimation = scaleXAnimationMidArea;
                        scaleYAnimation = scaleYAnimationMidArea;
                        break;
                    case CollisionArea.secondLevel:
                        scaleXAnimation = scaleXAnimation2ndArea;
                        scaleYAnimation = scaleYAnimation2ndArea;
                        break;
                    case CollisionArea.thirdLevel:
                        scaleXAnimation = scaleXAnimation3rdArea;
                        scaleYAnimation = scaleYAnimation3rdArea;
                        break;
                    default:
                        throw new NotImplementedException();  // Added a new CollisionArea
                }

                collisionStoryboard.Children.Add(scaleXAnimation);
                collisionStoryboard.Children.Add(scaleYAnimation);

                Storyboard.SetTarget(scaleXAnimation, item);
                Storyboard.SetTargetProperty(scaleXAnimation, new PropertyPath(ScatterViewItem.WidthProperty));
                Storyboard.SetTarget(scaleYAnimation, item);
                Storyboard.SetTargetProperty(scaleYAnimation, new PropertyPath(ScatterViewItem.HeightProperty));

                // prepare fade out of the explosion image
                Storyboard.SetTarget(fadeOutAnimation, explosionImage);
                Storyboard.SetTargetProperty(fadeOutAnimation, new PropertyPath(Image.OpacityProperty));

                return item;
            }

            private ScatterViewItem CreateAnimatedScoreSVI(int collisionScore)
            {
                // create textbox and viewbox to display auto scaling score
                TextBox scoreText = new TextBox();
                scoreText.Foreground = new SolidColorBrush(Colors.White);
                scoreText.Text = collisionScore.ToString();
                scoreText.Background = new SolidColorBrush(Colors.Transparent);
                scoreText.BorderThickness = new Thickness(0);
                Viewbox scoreBox = new Viewbox();
                scoreBox.Child = scoreText;
                scoreBox.IsHitTestVisible = false;
                scoreText.IsHitTestVisible = false;

                // create scatterviewitem and set its content to score. the svi serves as score container
                ScatterViewItem scoreItem = CreateSVIContainer();
                scoreItem.Content = scoreBox;
                scoreItem.CanMove = false;
                scoreItem.CanRotate = false;
                scoreItem.CanScale = false;
                scoreItem.IsHitTestVisible = false;

                // prepare fade out and scale of the score container
                Storyboard.SetTarget(fadeScoreOutAnimation, scoreText);
                Storyboard.SetTargetProperty(fadeScoreOutAnimation, new PropertyPath(TextBox.OpacityProperty));
                Storyboard.SetTarget(scaleScoreYAnimation, scoreItem);
                Storyboard.SetTargetProperty(scaleScoreYAnimation, new PropertyPath(ScatterViewItem.HeightProperty));
                Storyboard.SetTarget(scaleScoreXAnimation, scoreItem);
                Storyboard.SetTargetProperty(scaleScoreXAnimation, new PropertyPath(ScatterViewItem.WidthProperty));

                return scoreItem;
            }


            private ScatterViewItem CreateSVIContainer()
            {
                ScatterViewItem item = new ScatterViewItem();

                item.CanMove = false;
                item.CanRotate = false;
                item.CanScale = false;
                item.Background = Brushes.Transparent;
                item.ZIndex = 100;

                // remove annoying shadow of scatterview item
                RoutedEventHandler loadedEventHandler = null;
                loadedEventHandler = new RoutedEventHandler(delegate
                {
                    item.Loaded -= loadedEventHandler;
                    Microsoft.Surface.Presentation.Generic.SurfaceShadowChrome ssc;
                    ssc = item.Template.FindName("shadow", item) as Microsoft.Surface.Presentation.Generic.SurfaceShadowChrome;
                    ssc.Visibility = Visibility.Hidden;
                });
                item.Loaded += loadedEventHandler;

                return item;
            }

        }


        /// <summary>
        /// Responsible for calculating the collision points
        /// </summary>
        class ProtonCollisionScoreCalculator
        {

            public static int CalculateCollisionScoreOf(Proton protonOne, Proton protonTwo, CollisionArea area)
            {
                // Of course the following calculation of the collision score is not physically correct 
                // but includes the speed and the rotation of the protons
                double v1 = protonOne.GetVelocityEnergy();
                double r1 = protonOne.GetRotationEnergy();

                double v2 = protonTwo.GetVelocityEnergy();
                double r2 = protonTwo.GetRotationEnergy();

                double collisionpoints = 500 * r1 + 500 * r2 + v1 / 300 + v2 / 300;

                return (int)(collisionpoints * area.CollisionScoreMultiplicator());
            }
        }

    }
}
