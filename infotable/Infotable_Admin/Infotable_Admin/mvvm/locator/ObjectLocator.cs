﻿using System;

namespace Infotable_Admin.mvvm.locator
{
    public static class ObjectLocator
    {
        private static ILocator locator = new ViewModelLocator();

        public static ILocator Current
        {
            get
            {
                return locator;
            }
        }

        public static void Register(ILocator locatorToRegister)
        {
            if (locatorToRegister == null)
                throw new ArgumentNullException("locatorToRegister");

            locator = locatorToRegister;
        }
    }
}
