﻿using Infotable_Admin.model;
using Infotable_Admin.model.enums;
using Infotable_Admin.service.log;
using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml;

namespace Infotable_Admin.service
{
    /// <summary>
    /// Ist für die Kommunikation zwischen XML Dateien und Programm zuständig.
    /// </summary>
    public static class XML
    {
        private static string configDatei;
        private static string verzeichnis;
        private static string personenDatei;
        private static string newsDatei;
        public static string medienVerzeichnis;

        /// <summary>
        /// setzt das Verzeichnis sowie die Personen- und News-Xml-Datei
        /// </summary>
        public static void SetDirectory()
        {
            configDatei = AppDomain.CurrentDomain.BaseDirectory + "config.xml";

            XmlDocument doc = new XmlDocument();
            doc.Load(configDatei);
            XmlElement root = doc.DocumentElement;

            XmlNodeList configSettings = root.ChildNodes;

            for (int i = 0; i < configSettings.Count; i++)
            {
                XmlNode attribut = configSettings.Item(i);

                switch (attribut.Name)
                {
                    case "verzeichnis": verzeichnis = attribut.InnerText + "\\"; break;
                    default: break;
                }
            }

            personenDatei = verzeichnis + "personen.xml";
            medienVerzeichnis = verzeichnis + "medien\\";
            newsDatei = verzeichnis + "news.xml";
            
            // wenn verzeichnis leer: ohne personen.xml, news.xml und ohne medien ordner
            try
            {
                if (!Directory.Exists(verzeichnis))
                    Directory.CreateDirectory(verzeichnis);

                if (!Directory.Exists(medienVerzeichnis))
                    Directory.CreateDirectory(medienVerzeichnis);

                bool person = false;
                bool news = false;

                string[] files = Directory.GetFiles(verzeichnis);
                for (int i = 0; i < files.Length; i++)
                {
                    if (files[i].Equals(personenDatei))
                        person = true;
                    if (files[i].Equals(newsDatei))
                        news = true;
                }

                if (!person)
                    createXML(Element.Person);
                if (!news)
                    createXML(Element.Neuigkeit);
            }
            catch (Exception ex)
            {
                LogWriter.writeLog(ex);
            }
        }

        /// <summary>
        /// liest alle Personen aus einem Dateisystem, bestehend aus XML Dateien aus.
        /// Inklusive der Bilder und Videos.
        /// </summary>
        /// <returns>die Liste aller Personen als ObservableCollection</returns>
        public static ObservableCollection<Person> getPersonen()
        {
            ArrayList personen = new ArrayList();

            XmlDocument doc = new XmlDocument();
            doc.Load(personenDatei);
            XmlElement root = doc.DocumentElement;

            XmlNodeList personenListe = root.ChildNodes;

            for (int i = 0; i < personenListe.Count; i++)
            {
                XmlNode personKnoten = personenListe.Item(i);

                Person person = new Person();
                person.Id = Convert.ToInt32(personKnoten.Attributes.GetNamedItem("id").InnerText);
                person.Verzeichnis = medienVerzeichnis + person.Id + "\\";

                XmlNodeList attribute = personKnoten.ChildNodes;

                for (int j = 0; j < attribute.Count; j++)
                {
                    XmlNode attribut = attribute.Item(j);

                    switch (attribut.Name)
                    {
                        case "vorname": person.Vorname = attribut.InnerText; break;
                        case "nachname": person.Nachname = attribut.InnerText; break;
                        case "geburtsdatum":
                            {
                                string[] gebdatumString = attribut.InnerText.Split('.');
                                if (gebdatumString.Length > 1)
                                    person.Geburtsdatum = new DateTime(Convert.ToInt32(gebdatumString[2]), Convert.ToInt32(gebdatumString[1]), Convert.ToInt32(gebdatumString[0]));
                                break;
                            }
                        case "sterbedatum":
                            {
                                string[] stedatumString = attribut.InnerText.Split('.');
                                if (stedatumString.Length > 1)
                                    person.Sterbedatum = new DateTime(Convert.ToInt32(stedatumString[2]), Convert.ToInt32(stedatumString[1]), Convert.ToInt32(stedatumString[0]));
                                break;
                            }
                        case "leben": person.Leben = attribut.InnerText; break;
                        case "werdegang": person.Werdegang = attribut.InnerText; break;
                        case "video":
                            {
                                person.VideoPfade.Add(new Datei(medienVerzeichnis + person.Id + "\\" + attribut.InnerText, medienVerzeichnis + person.Id, attribut.InnerText, Data.Video));
                                break;
                            }
                        case "bild":
                            {
                                person.BilderPfade.Add(new Datei(medienVerzeichnis + person.Id + "\\" + attribut.InnerText, medienVerzeichnis + person.Id, attribut.InnerText, Data.Bild));
                                break;
                            }
                        default: break;
                    }
                }

                person.SetName();
                person.Change = true;
                personen.Add(person);
            }

            // sortiere Liste
            personen.Sort();

            ObservableCollection<Person> result = new ObservableCollection<Person>();
            for (int i = 0; i < personen.Count; i++)
            {
                result.Add((Person)personen[i]);
            }

            return result;
        }

        /// <summary>
        /// Liest alle News aus der XML-Datei news.xml aus.
        /// </summary>
        /// <returns>Die Liste aller News als ObservableCollection</returns>
        public static ObservableCollection<News> getNeuigkeiten()
        {
            ArrayList neuigkeiten = new ArrayList();

            XmlDocument doc = new XmlDocument();
            doc.Load(newsDatei);
            XmlElement root = doc.DocumentElement;

            XmlNodeList artikelList = root.ChildNodes;

            for (int i = 0; i < artikelList.Count; i++)
            {
                XmlNode artikel = artikelList.Item(i);

                News neuigkeit = new News();
                neuigkeit.Id = Convert.ToInt32(artikel.Attributes.GetNamedItem("id").InnerText);

                XmlNodeList childNodes = artikel.ChildNodes;

                for (int j = 0; j < childNodes.Count; j++)
                {
                    XmlNode child = childNodes.Item(j);

                    switch (child.Name)
                    {
                        case "titel": neuigkeit.Titel = child.InnerText; break;
                        case "inhalt": neuigkeit.Inhalt = child.InnerText; break;
                        default: break;
                    }
                }

                neuigkeit.Change = true;
                neuigkeiten.Add(neuigkeit);
            }

            // sortiere Liste
            neuigkeiten.Sort();

            ObservableCollection<News> result = new ObservableCollection<News>();
            for (int i = 0; i < neuigkeiten.Count; i++)
            {
                result.Add((News)neuigkeiten[i]);
            }

            return result;
        }

        /// <summary>
        /// Fügt ein neues Element in der XML-Datei hnzu.
        /// Person oder News
        /// </summary>
        /// <param name="id">neu erzeugte ID</param>
        /// <param name="element">enum Element</param>
        public static void addElement(int id, Element element)
        {
            string xmlFile = getFilename(element);

            if (xmlFile.Equals(string.Empty))
                return;

            XmlDocument doc = new XmlDocument();
            doc.Load(xmlFile);

            XmlNode root = doc.DocumentElement;

            XmlNode newNode = null;

            if (element == Element.Person)
            {
                newNode = doc.CreateElement("person");

                XmlAttribute idAttr = doc.CreateAttribute("id");
                idAttr.InnerText = id.ToString();
                newNode.Attributes.Append(idAttr);

                XmlNode vorname = doc.CreateElement("vorname");
                newNode.AppendChild(vorname);

                XmlNode nachname = doc.CreateElement("nachname");
                newNode.AppendChild(nachname);

                XmlNode geburtsdatum = doc.CreateElement("geburtsdatum");
                newNode.AppendChild(geburtsdatum);

                XmlNode sterbedatum = doc.CreateElement("sterbedatum");
                newNode.AppendChild(sterbedatum);

                XmlNode leben = doc.CreateElement("leben");
                newNode.AppendChild(leben);

                XmlNode werdegang = doc.CreateElement("werdegang");
                newNode.AppendChild(werdegang);
            }
            else if (element == Element.Neuigkeit)
            {
                newNode = doc.CreateElement("artikel");

                XmlAttribute idAttr = doc.CreateAttribute("id");
                idAttr.InnerText = id.ToString();
                newNode.Attributes.Append(idAttr);

                XmlNode titel = doc.CreateElement("titel");
                newNode.AppendChild(titel);

                XmlNode inhalt = doc.CreateElement("inhalt");
                newNode.AppendChild(inhalt);
            }

            root.AppendChild(newNode);

            doc.Save(xmlFile);
        }

        /// <summary>
        /// Verändert den InnerText eines XmlNodes.
        /// </summary>
        /// <param name="id">ID des Elements</param>
        /// <param name="element">enum Element</param>
        /// <param name="name">XmlNode in XML-Datei.
        /// Bei Person: "vorname", "nachname", "geburtsdatum", "sterbedatum", "leben", "werdegang".
        /// Bei Neuigkeit: "titel", "inhalt"</param>
        /// <param name="newValue">neuer Wert</param>
        /// <returns></returns>
        public static void editValue(int id, Element element, string name, string newValue)
        {
            string xmlFile = getFilename(element);

            if (xmlFile.Equals(string.Empty))
                return;

            XmlDocument doc = new XmlDocument();
            doc.Load(xmlFile);

            XmlNode root = doc.DocumentElement;

            XmlNodeList datenListe = root.ChildNodes;
            for (int i = 0; i < datenListe.Count; i++)
            {
                XmlNode daten = datenListe.Item(i);
                if (daten.Attributes.GetNamedItem("id").InnerText.Equals(id.ToString()))
                {
                    XmlNodeList attribute = daten.ChildNodes;
                    for (int j = 0; j < attribute.Count; j++)
                    {
                        XmlNode attribut = attribute.Item(j);
                        if(attribut.Name.Equals(name))
                        {
                            attribut.InnerText = newValue;
                            break;
                        }
                    }
                    break;
                }
            }

            doc.Save(xmlFile);
        }

        /// <summary>
        /// Entfernt ein XmlNode aus der XmlDatei.
        /// </summary>
        /// <param name="id">Die ID des Elementes.</param>
        /// <param name="element">enum Element</param>
        public static void removeElement(int id, Element element)
        {
            string xmlFile = getFilename(element);

            if (xmlFile.Equals(string.Empty))
                return;

            XmlDocument doc = new XmlDocument();
            doc.Load(xmlFile);

            XmlNode root = doc.DocumentElement;

            XmlNodeList datenListe = root.ChildNodes;

            for (int i = 0; i < datenListe.Count; i++)
            {
                XmlNode temp = datenListe[i];

                if (temp.Attributes.GetNamedItem("id").InnerText.Equals(id.ToString()))
                {
                    root.RemoveChild(temp);
                    break;
                }
            }

            doc.Save(xmlFile);
        }

        /// <summary>
        /// Erzeugt neue XML-Dateien in einem leeren Verzeichnis.
        /// </summary>
        /// <param name="element">enum Element</param>
        public static void createXML(Element element)
        {
            string xmlFile = getFilename(element);

            if (xmlFile.Equals(string.Empty))
                return;

            XmlDocument doc = new XmlDocument();

            XmlDeclaration decl = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(decl);

            XmlNode root = null;

            if (element == Element.Person)
            {
                root = doc.CreateElement("personen");
                doc.AppendChild(root);
            }
            else if (element == Element.Neuigkeit)
            {
                root = doc.CreateElement("artikel");
                doc.AppendChild(root);
            }


            doc.Save(xmlFile);
        }

        /// <summary>
        /// Fügt eine neue Datei in der XML-Datei der zugehörigen ID hinzu.
        /// </summary>
        /// <param name="id">ID der Person</param>
        /// <param name="datei">"bild" | "video"</param>
        /// <param name="dateiname">Der einzufügende Dateiname in dem neuen Element.</param>
        public static void addDatei(int id, string datei, string dateiname)
        {
            string xmlFile = personenDatei;

            if (xmlFile.Equals(string.Empty))
                return;

            XmlDocument doc = new XmlDocument();
            doc.Load(xmlFile);

            XmlNode root = doc.DocumentElement;

            XmlNodeList personenListe = root.ChildNodes;
            for (int i = 0; i < personenListe.Count; i++)
            {
                XmlNode person = personenListe.Item(i);
                if (person.Attributes.GetNamedItem("id").InnerText.Equals(id.ToString()))
                {
                    XmlNode newNode = doc.CreateElement(datei);
                    newNode.InnerText = dateiname;
                    person.AppendChild(newNode);
                    break;
                }
            }

            doc.Save(xmlFile);
        }

        /// <summary>
        /// Änder den Datei Pfad eines Bildes oder Videos.
        /// </summary>
        /// <param name="id">ID der Person</param>
        /// <param name="datei">"bild" | "video"</param>
        /// <param name="alterDateiname">Der alte  Dateiname des "Bildes" oder "Videos".</param>
        /// <param name="neuerDateiname">Der neue Dateiname für das "Bild" oder "Video".</param>
        public static void editDateiPfad(int id, string datei, string alterDateiname, string neuerDateiname)
        {
            string xmlFile = personenDatei;

            if (xmlFile.Equals(string.Empty))
                return;

            XmlDocument doc = new XmlDocument();
            doc.Load(xmlFile);

            XmlNode root = doc.DocumentElement;

            XmlNodeList personenListe = root.ChildNodes;
            for (int i = 0; i < personenListe.Count; i++)
            {
                XmlNode person = personenListe.Item(i);
                if (person.Attributes.GetNamedItem("id").InnerText.Equals(id.ToString()))
                {
                    XmlNodeList attribute = person.ChildNodes;
                    for (int j = 0; j < attribute.Count; j++)
                    {
                        XmlNode attribut = attribute[j];
                        if (attribut.Name == datei)
                        {
                            if (attribut.InnerText.Equals(alterDateiname))
                            {
                                attribut.InnerText = neuerDateiname;
                                break;
                            }
                        }
                    }
                    break;
                }
            }

            doc.Save(xmlFile);
        }

        /// <summary>
        /// Entfernt eine Datei aus der XML-Datei.
        /// Hier: "bild" | "video"
        /// </summary>
        /// <param name="id">ID der Person</param>
        /// <param name="datei">"bild" | "video"</param>
        /// <param name="dateiname">Der zu löschende Dateiname.</param>
        public static void removeDatei(int id, string datei, string dateiname)
        {
            string xmlFile = personenDatei;

            if (xmlFile.Equals(string.Empty))
                return;

            XmlDocument doc = new XmlDocument();
            doc.Load(xmlFile);

            XmlNode root = doc.DocumentElement;

            XmlNodeList personenListe = root.ChildNodes;
            for (int i = 0; i < personenListe.Count; i++)
            {
                XmlNode person = personenListe.Item(i);
                if (person.Attributes.GetNamedItem("id").InnerText.Equals(id.ToString()))
                {
                    XmlNodeList attribute = person.ChildNodes;
                    for (int j = 0; j < attribute.Count; j++)
                    {
                        XmlNode attribut = attribute[j];
                        if (attribut.Name == datei)
                        {
                            if (attribut.InnerText.Equals(dateiname))
                            {
                                person.RemoveChild(attribut);
                                break;
                            }
                        }
                    }
                    break;
                }
            }

            doc.Save(xmlFile);
        }

        /// <summary>
        /// Gibt den Dateipfad anhand des "Elements" zurück.
        /// </summary>
        /// <param name="element">enum Element</param>
        /// <returns></returns>
        private static string getFilename(Element element)
        {
            string xmlFile = string.Empty;
            if (element == Element.Person)
                xmlFile = personenDatei;
            else if (element == Element.Neuigkeit)
                xmlFile = newsDatei;

            return xmlFile;
        }
    }
}
