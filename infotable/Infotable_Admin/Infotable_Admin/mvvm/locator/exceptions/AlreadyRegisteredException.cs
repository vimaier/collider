﻿using System;
using System.Runtime.Serialization;

namespace Infotable_Admin.mvvm.locator.exceptions
{
    public class AlreadyRegisteredException : Exception
    {
        public AlreadyRegisteredException() { }

        public AlreadyRegisteredException(string message)
            : base(message)
        {
        }

        public AlreadyRegisteredException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected AlreadyRegisteredException(SerializationInfo serializationInfo, StreamingContext context)
            : base(serializationInfo, context)
        {
        }
    }
}
