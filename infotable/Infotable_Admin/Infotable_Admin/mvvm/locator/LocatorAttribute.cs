﻿using System;

namespace Infotable_Admin.mvvm.locator
{
    public class LocatorAttribute : Attribute
    {
        public string Name { get; set; }

        public LocatorAttribute(string name)
        {
            Name = name;
        }
    }
}
