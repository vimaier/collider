﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ProtonCollider.Controller;

namespace ProtonCollider.View
{
    /// <summary>
    /// Interaktionslogik für Info.xaml
    /// </summary>
    public partial class Info : UserControl
    {
        public Info()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Wechselt die View zum Hauptmenue
        /// </summary>
        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            ViewManager.Instance.ChangeView(ViewManager.Views.MainMenu);
        }

        #region BEGIN REGION: Change vertical scrollposition of the ScrollViewer by Mousedrag / Touchdrag

        //current scroll position
        Point scrollPoint = new Point();
        double verOffset = 1;

        /// <summary>
        /// Is Called on MouseDown at the ScrollViewer
        /// Captures the Mouse and enables the Scrollview to be dragged (on mouse move)
        /// </summary>
        private void ScrollViewer_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            MyScrollViewer.CaptureMouse();
            scrollPoint = e.GetPosition(MyScrollViewer);
            verOffset = MyScrollViewer.VerticalOffset;
        }

        /// <summary>
        /// Is Called on MouseMove at the ScrollViewer
        /// Changes the vertical scrollposition of the Scrollviewer relative to the mouse movement
        /// </summary>
        private void ScrollViewer_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (MyScrollViewer.IsMouseCaptured)
            {
                MyScrollViewer.ScrollToVerticalOffset(verOffset + (scrollPoint.Y - e.GetPosition(MyScrollViewer).Y));
            }
        }

        /// <summary>
        /// Is Called on MouseUp at the ScrollViewer
        /// Releases the Mousecapture in order to no longer drag the ScrollView on mouse move
        /// </summary>
        private void ScrollViewer_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            MyScrollViewer.ReleaseMouseCapture();
        }

        /// <summary>
        /// Is Called on TouchDown at the ScrollViewer
        /// Captures the Touch and enables the Scrollview to be dragged (on touch move)
        /// </summary>
        private void ScrollViewer_PreviewTouchDown(object sender, TouchEventArgs e)
        {
            MyScrollViewer.CaptureTouch(e.GetTouchPoint(MyScrollViewer).TouchDevice);
            scrollPoint = e.GetTouchPoint(MyScrollViewer).Position;
            verOffset = MyScrollViewer.VerticalOffset;
        }

        /// <summary>
        /// Is Called on TouchMove at the ScrollViewer
        /// Changes the vertical scrollposition of the Scrollviewer relative to the touch movement
        /// </summary>
        private void ScrollViewer_PreviewTouchMove(object sender, TouchEventArgs e)
        {
            if (MyScrollViewer.AreAnyTouchesCaptured)
            {
                MyScrollViewer.ScrollToVerticalOffset(verOffset + (scrollPoint.Y - e.GetTouchPoint(MyScrollViewer).Position.Y));
            }
        }

        /// <summary>
        /// Is Called on TouchUp at the ScrollViewer
        /// Releases the Touchcapture in order to no longer drag the ScrollView on touch move
        /// </summary>
        private void ScrollViewer_PreviewTouchUp(object sender, TouchEventArgs e)
        {
            MyScrollViewer.ReleaseTouchCapture(e.GetTouchPoint(MyScrollViewer).TouchDevice);
        }
        #endregion END REGION: Change Scrollposition by Mousedrag / Touchdrag
    }
}
