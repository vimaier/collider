﻿using Infotable_Admin.mvvm.model;
using Infotable_Admin.service;
using System;

namespace Infotable_Admin.model
{
    public class News : ModelBase, IComparable
    {
        #region Members

        private int id;
        private string titel;
        private string inhalt;

        // Ist dieses Attribut auf "true" gesetzt, werden Properties beim Neusetzen in der XML-Datei gespeichert
        // Ist dieses Attribut auf "false" gesetzt, so werden keine Änderungen an der XML-Datei vorgenommen.
        private bool change;
        
        #endregion

        #region Constructors
        public News()
        {
            this.Clear();
        }

        #endregion

        #region Properties

        public int Id
        {
            get { return id; }
            set
            {
                if (id == value)
                    return;
                id = value;
                RaisePropertyChanged("Id");
            }
        }

        public string Titel
        {
            get { return titel; }
            set
            {
                if (titel == value)
                    return;
                titel = value;
                RaisePropertyChanged("Titel");

                if (this.Change)
                {
                    XML.editValue(this.id, enums.Element.Neuigkeit, "titel", value);
                }
            }
        }

        public string Inhalt
        {
            get { return inhalt; }
            set
            {
                if (inhalt == value)
                    return;
                inhalt = value;
                RaisePropertyChanged("Inhalt");

                if (this.Change)
                {
                    XML.editValue(this.id, enums.Element.Neuigkeit, "inhalt", value);
                }
            }
        }

        public bool Change
        {
            get { return change; }
            set
            {
                if (change == value)
                    return;
                change = value;
            }
        }

        #endregion

        #region Methods

        public void Clear()
        {
            this.id = 0;
            this.titel = string.Empty;
            this.inhalt = string.Empty;
            this.change = false;
        }

        public int CompareTo(object o)
        {
            News n = (News)o;
            return String.Compare(this.titel, n.titel);
        }

        #endregion
    }
}
