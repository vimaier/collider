﻿using Infotable.mvvm.model;
using System;

namespace Infotable.model
{
    public class News : ModelBase, IComparable
    {
        #region Members

        private int id;
        private string titel;
        private string inhalt;
        
        #endregion

        #region Constructors

        public News()
        {
            this.Clear();
        }

        #endregion

        #region Properties

        public int Id
        {
            get { return id; }
            set
            {
                if (id == value)
                    return;
                id = value;
                RaisePropertyChanged("Id");
            }
        }

        public string Titel
        {
            get { return titel; }
            set
            {
                if (titel == value)
                    return;
                titel = value;
                RaisePropertyChanged("Titel");
            }
        }

        public string Inhalt
        {
            get { return inhalt; }
            set
            {
                if (inhalt == value)
                    return;
                inhalt = value;
                RaisePropertyChanged("Inhalt");
            }
        }

        #endregion

        #region Methods

        public void Clear()
        {
            this.id = 0;
            this.titel = string.Empty;
            this.inhalt = string.Empty;
        }

        public int CompareTo(object o)
        {
            News n = (News)o;
            return String.Compare(this.titel, n.titel);
        }

        #endregion
    }
}
