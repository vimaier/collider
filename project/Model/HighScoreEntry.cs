﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace ProtonCollider.Model
{
    /// <summary>
    /// Datenklasse / Modelklasse der Einzelnen Highscoreeintraege
    /// </summary>
    public class HighScoreEntry
    {
        private int rank;
        private string name;
        private int score;

        public HighScoreEntry()
        {
            name = "Unbekannt";
            score = 0;
            rank = 0;
        }

        public HighScoreEntry(string name, int score)
        {
            this.name = name;
            this.score = score;
        }

        public HighScoreEntry(string name, int score, int rank)
        {
            this.name = name;
            this.score = score;
            this.rank = rank;
        }

        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        public int Score
        {
            get { return this.score; }
            set { this.score = value; }
        }

        public int Rank
        {
            get { return this.rank; }
            set { this.rank = value; }
        }

        [XmlIgnore]
        public string RankString
        {
            get { return this.rank + "."; }
            set { }
        }
    }
}
