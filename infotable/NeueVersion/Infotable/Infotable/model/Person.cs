﻿using Infotable.mvvm.model;
using System;
using System.Collections.ObjectModel;

namespace Infotable.model
{
    public class Person : ModelBase, IComparable
    {
        #region Members

        private int id;
        private string nachname;
        private string vorname;
        private DateTime geburtsdatum;
        private DateTime sterbedatum;
        private string leben;
        private string werdegang;
        private ObservableCollection<Datei> bilderPfade;
        private ObservableCollection<Datei> videoPfade;

        // pics and videos together
        private ObservableCollection<Datei> mediaPfade;

        // displayMemberPath in ListView
        private string name;
        // displayMemberPath Date
        private string datum;

        // der Pfad indem sich Bilder und Videos befinden
        private string verzeichnis;

        // das Profilbild der Liste
        public Datei profilBild;
        
        #endregion

        #region Constructors

        public Person()
        {
            this.Clear();
        }

        #endregion

        #region Properties

        public int Id
        {
            get { return id; }
            set
            {
                if (id == value)
                    return;
                id = value;
                RaisePropertyChanged("Id");
            }
        }

        public string Nachname
        {
            get { return nachname; }
            set
            {
                if (nachname == value)
                    return;
                nachname = value;
                RaisePropertyChanged("Nachname");

                this.SetName();
            }
        }

        public string Vorname
        {
            get { return vorname; }
            set
            {
                if (vorname == value)
                    return;
                vorname = value;
                RaisePropertyChanged("Vorname");

                this.SetName();
            }
        }

        public DateTime Geburtsdatum
        {
            get { return geburtsdatum; }
            set
            {
                if (geburtsdatum == value)
                    return;
                geburtsdatum = value;
                RaisePropertyChanged("Geburtsdatum");

                if (value != null)
                    this.SetDatum();
            }
        }

        public DateTime Sterbedatum
        {
            get { return sterbedatum; }
            set
            {
                if (sterbedatum == value)
                    return;
                sterbedatum = value;
                RaisePropertyChanged("Sterbedatum");

                if (value != null)
                    this.SetDatum();
            }
        }

        public string Leben
        {
            get { return leben; }
            set
            {
                if (leben == value)
                    return;
                leben = value;
                RaisePropertyChanged("Leben");
            }
        }

        public string Werdegang
        {
            get { return werdegang; }
            set
            {
                if (werdegang == value)
                    return;
                werdegang = value;
                RaisePropertyChanged("Werdegang");
            }
        }

        public ObservableCollection<Datei> BilderPfade
        {
            get { return bilderPfade; }
            set
            {
                if (bilderPfade == value)
                    return;
                bilderPfade = value;
                RaisePropertyChanged("BilderPfade");
            }
        }

        public ObservableCollection<Datei> VideoPfade
        {
            get { return videoPfade; }
            set
            {
                if (videoPfade == value)
                    return;
                videoPfade = value;
                RaisePropertyChanged("VideoPfade");
            }
        }

        public ObservableCollection<Datei> MediaPfade
        {
            get { return mediaPfade; }
            set
            {
                if (mediaPfade == value)
                    return;
                mediaPfade = value;
                RaisePropertyChanged("MediaPfade");
            }
        }

        public string Name
        {
            get { return name; }
            set
            {
                if (name == value)
                    return;
                name = value;
                RaisePropertyChanged("Name");
            }
        }

        public string Datum
        {
            get { return datum; }
            set
            {
                if (datum == value)
                    return;
                datum = value;
                RaisePropertyChanged("Datum");
            }
        }

        public string Verzeichnis
        {
            get { return verzeichnis; }
            set
            {
                if (verzeichnis == value)
                    return;
                verzeichnis = value;
                RaisePropertyChanged("Verzeichnis");
            }
        }

        public Datei ProfilBild
        {
            get { return profilBild; }
            set
            {
                if (profilBild == value)
                    return;
                profilBild = value;
                RaisePropertyChanged("ProfilBild");
            }
        }

        #endregion

        #region Methods

        public void Clear()
        {
            this.id = 0;
            this.nachname = string.Empty;
            this.vorname = string.Empty;
            this.geburtsdatum = new DateTime();
            this.sterbedatum = new DateTime();
            this.leben = string.Empty;
            this.werdegang = string.Empty;
            this.bilderPfade = new ObservableCollection<Datei>();
            this.videoPfade = new ObservableCollection<Datei>();
            this.mediaPfade = new ObservableCollection<Datei>();
            this.name = string.Empty;
            this.datum = string.Empty;
            this.verzeichnis = string.Empty;
            this.profilBild = new Datei();
        }

        public void SetName()
        {
            if (this.vorname.Equals(string.Empty) && this.nachname.Equals(string.Empty))
                this.Name = "empty";
            else if (this.vorname.Equals(string.Empty))
                this.Name = this.nachname;
            else if (this.nachname.Equals(string.Empty))
                this.Name = this.vorname;
            else
                this.Name = this.vorname + " " + this.nachname;
        }

        public void SetDatum()
        {
            if (this.geburtsdatum == null || this.sterbedatum == null)
                return;

            if (this.geburtsdatum.Year == 1 && this.sterbedatum.Year == 1)
                this.Datum = string.Empty;
            else if (this.geburtsdatum.Year != 1 && this.sterbedatum.Year == 1)
                this.Datum = "*" + this.geburtsdatum.ToShortDateString();
            else if (this.geburtsdatum.Year == 1 && this.sterbedatum.Year != 1)
                this.Datum = this.sterbedatum.ToShortDateString();
            else
                this.Datum = "*" + this.geburtsdatum.ToShortDateString() + " - †" + this.sterbedatum.ToShortDateString();
        }

        public void SetMediaPfade()
        {
            if (this.BilderPfade == null && this.VideoPfade == null)
                return;

            this.MediaPfade.Clear();

            for (int i = 0; i < this.BilderPfade.Count; i++)
            {
                this.MediaPfade.Add(this.BilderPfade[i]);
            }

            for (int i = 0; i < this.VideoPfade.Count; i++)
            {
                this.MediaPfade.Add(this.VideoPfade[i]);
            }
        }

        public int CompareTo(object o)
        {
            Person p = (Person)o;
            return DateTime.Compare(this.geburtsdatum, p.geburtsdatum);
        }

        #endregion
    }
}
